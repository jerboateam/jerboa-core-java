package up.jerboa.algo;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import up.jerboa.algo.object.SigData;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaIslet;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.util.StopWatch;

public class JerboaAlgorithm {

	
	// 0 : indetermine
	// 1 : false
	// 2 : true
	// 3 (ou autre) errors
	// IDX1 -> kind  IDX2 -> vkind (kind du voisin)
	private static byte[][] spread_value_orient = { 
			{ 0, 2, 1, 0 }, 
			{ 1, 3, 1, 1 }, 
			{ 2, 2, 3, 2 }, 
			{ 3, 3, 3, 3 } 
	};
	
	/**
	 * This function regenerates orientation for a gmap. The regeneration is effective if and only if no error
	 * occurs during regeneration of orientation. This function represents orientation has two values (positive
	 * and negative orientation). This value is setted in an embedding value inside the gmap. This function
	 * returns true if the orientation succeed and false otherwise. The latter ensures no modification in the gmap.
	 * @param <T> type of value besides orientation (e.g. boolean)
	 * @param gmap structure of gmap 
	 * @param ebdorientid embedding that will store orientation value.
	 * @param valtrue instance of the positive orientation.
	 * @param valfalse instance of the negative orientation.
	 * @return True when the regeneration succeed, and false otherwise (no modification occur in gmap)
	 */
	public static <T> boolean regenOrient(JerboaGMap gmap, int ebdorientid, T valtrue, T valfalse) {
		StopWatch sw = new StopWatch();
		List<Integer> isletsCC = JerboaIslet.islet_par(gmap, JerboaOrbit.orbit(0,1,2,3));
		Set<Integer> referees = new HashSet<>(isletsCC);
		isletsCC = new ArrayList<>(referees);
		sw.display("end of computation of connex compound!");
		
		byte[] resorient = spreadOrient(gmap, isletsCC);
		sw.display("end raw computation of orient");
		
		long errors = 0;
		for(byte b : resorient)
			if(b==0 || b==3)
				errors++;
		
		sw.display("end check object is orientable (should = 0): " + errors);
		if(errors == 0) {
			gmap.parallelStream().forEach(d -> {
				int did = d.getID();
				if(resorient[did] == 1) // false
					d.setEmbedding(ebdorientid, valfalse);
				else
					d.setEmbedding(ebdorientid, valtrue);
			});
			sw.display("end affectation");
			
			return true;
		}
		else
			return false;
	}

	/**
	 * This function compute the right orientation of a gmap. This function return an array of byte corresponding
	 * to the orientation flag for each dart. Be careful, the result includes errors. This function must be handled 
	 * very carefully. 
	 * @see JerboaAlgorithm#regenOrient(JerboaGMap, int, Object, Object) more understandable function
	 * @param gmap describe the gmap structure
	 * @param dartsplus list of dart with positive orientation.
	 * @return an array of the size of the gmap where value is as follow: 
	 * <ul>
	 * 	<li>0 : indeterminate orientation</li>
	 * 	<li>1 : orientation is at false value</li>
	 * 	<li>2 : orientation is at true value </li>
	 * 	<li>3 : errors or any trouble</li>
	 * </ul>
	 */
	private static byte[] spreadOrient(JerboaGMap gmap, List<Integer> dartsplus) {
		final int size = gmap.getLength();
		final int dim = gmap.getModeler().getDimension();
		byte[] tmp = new byte[size];
		// 0 : indetermine
		// 1 : false
		// 2 : true
		// 3 (ou autre) errors
		
		for (int jerboaDart : dartsplus) {
			tmp[jerboaDart] = 2;
		}
		
		boolean cont = true;
		while(cont) {
			int totalmodif = IntStream.range(0, size).parallel().map(i -> {
				int modif = 0;
				JerboaDart d = gmap.getNode(i);
				byte kind = tmp[i];
				for(int a = 0; a <= dim; a++) {
					JerboaDart vd = d.alpha(a);
					if(d != vd) {
						byte vkind = tmp[vd.getID()];
						byte prevkind = kind;
						kind = spread_value_orient[kind][vkind];
						if(prevkind == 0 && kind != 0) {
							tmp[i] = kind;
							modif++;
						}
					}
				}
				return modif;
			}).sum();
			// System.out.println("SPREAD step modified on " + totalmodif+ " darts");
			cont = (totalmodif>0);
		}
		return tmp;
	}
	
	public static List<Integer> gmapSignature(JerboaModeler modeler, JerboaGMap gmap) {
		List<List<Integer>> sigs = gmapToWords(modeler, gmap);
		
		sigs.sort(new Comparator<List<Integer>>() {
			@Override
			public int compare(List<Integer> o1, List<Integer> o2) {
				int s = o1.size();
				for(int i = 0;i < (s-1); ++i) {
					if(o1.get(i).intValue() > o2.get(i).intValue()) {
						return 1;
					}
				}
				return Integer.compare(o1.get(s-1).intValue(), o2.get(s-1).intValue());
			}
		});
		
		if(sigs.size() == 0)
			return new ArrayList<>();
		else
			return sigs.get(0);
	}
	
	public static List<List<Integer>> gmapToWords(JerboaModeler modeler, JerboaGMap gmap) {
		List<List<Integer>> res = new ArrayList<>();
		
		return gmap.parallelStream().map(start -> gmapToWordStep(modeler, gmap, start)).collect(Collectors.toList());
		/*for (JerboaDart dart : gmap) {
			List<Integer> sig = gmapToWordStep(modeler, gmap, dart);
			res.add(sig);
		}
		return res;*/
	}
	
	public static List<Integer> gmapToWordStep(JerboaModeler modeler, JerboaGMap gmap, JerboaDart start) {
		int dim = modeler.getDimension();
		Deque<JerboaDart> file = new ArrayDeque<>();
		int tag = 0;
		file.addLast(start);
		List<Integer> sig = IntStream.range(0, gmap.getLength()).map(i -> -1).boxed().collect(Collectors.toList());
		sig.set(start.getID(), 1);
		tag = 2;
		List<Integer> res = new ArrayList<>();
		while(!file.isEmpty()) {
			JerboaDart dart = file.removeFirst();
			for(int i = 0; i <= dim; ++i) {
				JerboaDart vdart = dart.alpha(i);
				int vid = vdart.getID();
				if(sig.get(vid) == -1) {
					file.addLast(vdart);
					sig.set(vid, tag++);
				}
			}
			
			res.add(sig.get(dart.getID()));
			res.add(sig.get(dart.alpha(0).getID()));
			res.add(sig.get(dart.alpha(1).getID()));
			res.add(sig.get(dart.alpha(2).getID()));
			res.add(sig.get(dart.alpha(3).getID()));
		}
		
		// add dans res, pour le brin dart, une fois parcouru les alphas pour les signe et il faut envoyer 
		// DARTID(tag) DARTID@0(tag) DARTID@1(tag)  DARTID@2(tag) DARTID@3(tag) 
		return res;
	}

	public static List<Integer> gmapSignature_par(JerboaModeler modeler, JerboaGMap gmap) {
		
		List<SigData> sigs = gmap.parallelStream().map(dart -> new SigData(gmap, dart)).collect(Collectors.toList());
		SigData ref = null;
		
		while(sigs.parallelStream().anyMatch(s -> !s.isEmpty())) {
			ref = sigs.parallelStream().map(s -> s.step()).min((a,b) -> a.compareTo(b)).orElse(null);
			{
				SigData cref = ref;
				int presize = sigs.size();
				sigs = sigs.parallelStream().filter(a -> a.compareTo(cref) <= 0).collect(Collectors.toList());
				int postsize = sigs.size();
				if(presize != postsize)
					System.err.println("diminution: "+presize+" -> "+postsize);
			}
			
		}
		return sigs.parallelStream().min((a,b) -> a.compareTo(b)).orElse(null).signature();
		// return ref == null? null : ref.signature();
		//return sigs.parallelStream().min((a,b) -> a.compareTo(b)).orElse(null).signature();// ref==null? null : ref.signature();
			}
	
}
