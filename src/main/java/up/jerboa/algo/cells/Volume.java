package up.jerboa.algo.cells;

import java.util.Iterator;
import java.util.Set;

public class Volume implements Iterable<Face>{
	
	private Set<Face> faces;

	public Volume(Set<Face> faces) {
		this.faces = faces;
	}

	public Set<Face> getFaces() {
		return faces;
	}
	
	public int size() {
		return faces.size();
	}

	@Override
	public Iterator<Face> iterator() {
		return faces.stream().iterator();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.size());
		for (Face face : faces) 
			builder.append(" " + face);
		return builder.toString();
	}
}
