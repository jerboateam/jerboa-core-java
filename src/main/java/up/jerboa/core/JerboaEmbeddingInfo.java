/**
 * 
 */
package up.jerboa.core;

import up.jerboa.exception.JerboaRuntimeException;


/**
 * This class encompasses all information about one embedding.In Jerboa, an embedding is defined by a name,
 * the support orbit, and the fullname type of the underlying representation. For optimization purpose, an embedding
 * has an ID when, it is registered in a modeler. This number is for internal use.
 * @author hakim
 *
 */
public class JerboaEmbeddingInfo {
	private String name;
	private JerboaOrbit orbit;
	private Class<?> type;
	private int id;
	
	
	/**
	 * 
	 */
	public JerboaEmbeddingInfo(String name, JerboaOrbit orbit, Class<?> type) {
		this.name = name;
		this.orbit = orbit;
		this.type = type;
		this.id = -1;
	}
	
	public JerboaEmbeddingInfo(String name, JerboaOrbit orbit, String className) throws ClassNotFoundException {
		this(name,orbit, Class.forName(className));
	}

	public JerboaOrbit getOrbit() {
		return orbit;
	}
	
	public String getName() {
		return name;
	}
	
	public int getID() {
		return id;
	}
	
	public Class<?> getType() {
		return type;
	}
	
	void registerID(int id) {
		if(this.id != -1 && this.id != id)
			throw new JerboaRuntimeException("Embedding seems shared between many modelers!: " + toString());
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof JerboaEmbeddingInfo) {
			final JerboaEmbeddingInfo jei = (JerboaEmbeddingInfo) obj;
			return name.equals(jei.getName());
		}
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Ebd Info: ");
		sb.append(name).append(" -> ").append(orbit.toString()).append(" : ").append(type);
		return sb.toString();
	}
}
