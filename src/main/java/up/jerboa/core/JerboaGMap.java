package up.jerboa.core;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import up.jerboa.core.util.JerboaGMapDuplicateFactory;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaGMapDuplicateException;
import up.jerboa.exception.JerboaMalFormedException;
import up.jerboa.exception.JerboaNoFreeTagException;

public interface JerboaGMap extends Iterable<JerboaDart> {

	/**
	 * Getter of the current capacity of the gmap.
	 * @return Return the current capacity.
	 */
	public int getCapacity();

	/**
	 * Getter of the current dimension of the gmap.
	 * @return The current dimension.
	 */
	public int getDimension();

	/**
	 * This function check if the index correspond to an existent node. The node must not been marked as
	 * deleted.
	 *
	 * <b>Complicity: constant</b>
	 *
	 * @param i searched index of the node
	 * @return Return true if the index of the node exist and false otherwise.
	 */
	public boolean existNode(int i);

	/**
	 * This function returns the node with the index i. In the case of attempts to non existent node,
	 * this function returns the null pointer.
	 *
	 * <b>Complexity: constant</b>
	 * @param i index of the searched node
	 * @return Return the node at the position i or null otherwise.
	 */
	public JerboaDart getNode(int i);

	/**
	 * This function add a new node in the current gmap and the return the created {@see JerboaNode}.
	 * If the gmap reaches the capacity, this method extends automatically the capacity.
	 * The complexity depends of the current state of the gmap, the most of time it will be constant,
	 * but in case of extension this method could take more time.
	 * @return Return the created node.
	 */
	public JerboaDart addNode();

	/**
	 * This functions create <i>size</i> nodes at onces.
	 * @param size the count of created node.
	 * @return an array that contains all created nodes.
	 */
	public JerboaDart[] addNodes(int size);

	/**
	 * This function deletes the node of index i.
	 * The complexity is constant.
	 * @param i index of the node to be deleted.
	 */
	public void delNode(int n);

	public void makeDelete(int n);

	// hyp: le noeud doit etre un noeud de la gmap en cours
	// on ne fait aucune verif a ce niveau
	public void delNode(JerboaDart n);

	/**
	 * This function make a set of checks in the current gmap:
	 * <ol>
	 * <li>check the coherence in the adjacent matrix</li>
	 * <li>check the coherence in the dimension of each node</li>
	 * <li>check the existence of neighbor for any existent node</li>
	 * <li>check the condition cycle</li>
	 * <li>check the coherence of node marked as deleted the delete pool</li>
	 * </ol>
	 * @return Return true and all checks are correct and false otherwise.
	 */
	public boolean check(boolean checkEbd);

	/**
	 * @throws JerboaMalFormedException
	 * @throws JerboaException
	 */
	public void deepCheck(boolean checkEbd) throws JerboaMalFormedException, JerboaException;

	public JerboaDart node(int i);

	/**
	 * Return the length of the GMap with the hole!! (this number could be greater that the real size of the GMAP)
	 * @return
	 * @see JerboaGMap#getCapacity()
	 * @see JerboaGMap#size()
	 */
	public int getLength();
	
	/**
	 * Return the size of the GMap (without the hole)!!
	 * @return
	 * @see JerboaGMap#getLength()
	 * @see JerboaGMap#getCapacity()
	 */
	public int size();
	
	public JerboaMark creatFreeMarker();
	public JerboaMark creatFreeMarker(boolean threadSafe);
	
	public void freeMarker(JerboaMark marker);
	public void mark(JerboaMark marker, JerboaDart cur);
	public void unmark(JerboaMark marker, JerboaDart cur);
	
	
	public List<JerboaDart> getMarkedNode(JerboaMark marker);

	/*
	 * sorb : orbite de plongement
	 * orb : orbite du parcours
	 */
	public List<JerboaDart> collect(JerboaDart start, JerboaOrbit orb,
			JerboaOrbit sorb) throws JerboaException;
	
	public List<JerboaDart> collectCustomMark(final JerboaDart start,final JerboaOrbit orb, final JerboaOrbit sorb, final JerboaMark markerOrb, final JerboaMark markerSorb) throws JerboaException; 

	public <T> List<T> collect(JerboaDart start, JerboaOrbit orb,
			JerboaOrbit sorb, String ebdname) throws JerboaException;
	
	public <T> List<T> collect(JerboaDart start, JerboaOrbit orb,
			JerboaOrbit sorb, int ebdID) throws JerboaException;

	public <T> List<T> collect(JerboaDart start, JerboaOrbit orbit, int ebdID)
			throws JerboaException;

	public <T> List<T> collect(JerboaDart start, JerboaOrbit orbit,
			String ebdname) throws JerboaException;

	public List<JerboaDart> orbit(JerboaDart start, JerboaOrbit orbit)
			throws JerboaException;

	public List<JerboaDart> markOrbit(JerboaDart start, JerboaOrbit orbit,
			JerboaMark marker) throws JerboaException;
	public List<JerboaDart> unmarkOrbit(JerboaDart start, JerboaOrbit orbit, JerboaMark markerA)
		throws JerboaException;
	
	public List<JerboaDart> markOrbitException(JerboaDart start, JerboaOrbit orbit, JerboaMark marker, 
			HashMap<JerboaDart, Set<Integer>> exceptions)
		throws JerboaException;

	public void clear();

	public String toString();

	public Iterator<JerboaDart> iterator();

	/**
	 * This function modifies the gmap by compacting all vertices of the gmap.
	 * Of course, this function does not modifies the topology.
	 */
	public void pack();
	
	public void swap(int aid, int bid);

	/**
	 * This function allows duplication of the current gmap into the argument.
	 * Caution, the criteria for allowing the duplication refers the same dimension
	 * between both GMap.
	 * Hint: you should pack the gmap before call this duplication, because holes are preserved
	 * in the inner structure.
	 *
	 * @param gmap: the gmap where making duplication
	 * @throws JerboaGMapDuplicateException: thrown when dimension mismatch
	 */
	public void duplicateInGMap(JerboaGMap gmap,
			JerboaGMapDuplicateFactory factory)
			throws JerboaGMapDuplicateException;

	public void ensureCapacity(int v);

	public JerboaModeler getModeler();

	void resetOrbitBuffer();
	
	public boolean hasInvariant();
	public List<String> getInvariantNames();
	
	/**
	 * Return all darts through a java sequential stream. With all description in Java 8.
	  * CAUTION: this stream returns only existent dart (called packed form), if you need to conserve hole (e.g.
	 * identifier of dart and position in a list) you must use the other function stream with an argument
	 * to false
	 * @see JerboaGMap#stream(boolean)
	* @return 
	 */
	public Stream<JerboaDart> stream();
	

	/**
	 * Return all darts through a java sequential stream. With all description in Java 8.
	 * @return 
	 */
	Stream<JerboaDart> stream(boolean pack);
	
	/**
	 * This function return a parallel stream of Java 8 Stream, with classical convention.
	 * In particular, if the parallel aspect is not possible, then a sequential stream is returned.
	 * CAUTION: this stream returns only existent dart (called packed form), if you need to conserve hole (e.g.
	 * identifier of dart and position in a list) you must use the other function parallelStream with an argument
	 * to false
	 * @see JerboaGMap#parallelStream(boolean)  
	 * @return a stream which attemps to be parallel (sequential otherwise).
	 */
	public Stream<JerboaDart> parallelStream();
	
	/**
	 * This function return a parallel stream of Java 8 Stream, with classical convention.
	 * In particular, if the parallel aspect is not possible, then a sequential stream is returned.
	 * @param pack indicates if the stream must return only existent dart or a stream of the length of the
	 * gmap, where hole return a null reference.
	 * @return a stream which attemps to be parallel (sequential otherwise).
	 */
	Stream<JerboaDart> parallelStream(boolean pack);
	
	/**
	 * Returns the maximal number of tag indexes that can be simultaneously used.
	 * 
	 * @return Maximal tag indexes capacity.
	 */
	int getTagCapacity();
	
	/**
	 * Returns a free tag index.
	 * The returned tag index is no longer free until it is freed.
	 * 
	 * If no tag index is free, an exception is raised.
	 * 
	 * @return Free tag index.
	 * @throws JerboaNoFreeTagException Exception raised if no tag index is free.
	 */
	int getFreeTag() throws JerboaNoFreeTagException;
	
	/**
	 * Free the specified index.
	 * 
	 * @param index Freed index.
	 */
	void freeTag(int index);

	public <T> void addDynEmbedding(JerboaEmbeddingInfo dynEbd, T initValue);

	public void delDynEmbedding(JerboaEmbeddingInfo dynEbd);
	
	
	/**
	 * This function modifies order or dart in the gmap. 
	 * But does not lose any topological relation or embedding).
	 * 
	 * Be careful! NO MARKER MUST BE <<IN USED>>!
	 */
	public void shuffle();


	
}