/**
 *
 */
package up.jerboa.core;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import up.jerboa.core.mark.JerboaMarkBalancedTree;
import up.jerboa.core.mark.JerboaMarkBitSet;
import up.jerboa.core.mark.JerboaMarkHashSet;
import up.jerboa.core.util.JerboaGMapDuplicateFactory;
import up.jerboa.core.util.JerboaTracer;
import up.jerboa.core.util.tag.TagManager;
import up.jerboa.core.util.tag.TagManagerFactory;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaGMapDuplicateException;
import up.jerboa.exception.JerboaMalFormedException;
import up.jerboa.exception.JerboaNoFreeMarkException;
import up.jerboa.exception.JerboaNoFreeTagException;
import up.jerboa.exception.JerboaOrbitIncompatibleException;
import up.jerboa.exception.JerboaRuntimeException;
import up.jerboa.util.StopWatch;

/**
 * This class is the main class of Topologic graph model.
 *
 * @author Hakim Belhaouari
 *
 */
public class JerboaGMapArray implements JerboaGMap {
	
	/** Preferred tag capacity. */
	private static final int PREFERRED_TAG_CAPACITY = 4;
	
	/** Array of all nodes */
	private ArrayList<JerboaDart> nodes;
	/** Variable reminds the dimension*/
	private int dimension;
	/** Variable designs the current capacity of the gmap (internal use)*/
	private int capacity;
	/** Variable contains the current length of the gmap */
	private volatile int length;
	/** List of deleted node of the gmap */
	private List<JerboaDart> deletePool;
	private JerboaModeler modeler;
	
	private BitSet idIntMarkers;
	
	/**
	 * Tag manager used by the gmap. 
	 * The actual type of the manager will determined by the {@link TagManagerFactory}.
	 */
	private final TagManager tagManager;

	private ArrayList<JerboaMark> intMarkers;
	private ArrayList<JerboaMark> extMarkers;
	private int idExtMarkers;

	/**
	 * Default constructor of a gmap of dimension 3
	 */
	public JerboaGMapArray(final JerboaModeler modeler) {
		this(modeler,1023);
	}

	/**
	 * Constructor of a gmap with specific dimension and capacity. For a best used of the capacity,
	 * you should initialize to a number with the form 2^n-1 where n is lesser than 32.
	 * 
	 * @param modeler Modeler using this gmap.
	 * @param capacity Force the initial capacity of the gmap.
	 */
	public JerboaGMapArray(final JerboaModeler modeler, final int capacity) {
		this(modeler, capacity, PREFERRED_TAG_CAPACITY);
	}
	
	/**
	 * Constructor of a gmap with specific dimension and capacity. For a best used of the capacity,
	 * you should initialize to a number with the form 2^n-1 where n is lesser than 32.
	 * 
	 * The tag capacity should be the smallest possible.
	 * It is up to the developer to determine what the smallest possible is.
	 *
	 * @param dimension dimension of the gmap
	 * @param capacity force the initial capacity of the gmap.
	 * @param tagCapacity Maximal number of tag that can be simultaneously used.
	 */
	public JerboaGMapArray(final JerboaModeler modeler, final int capacity, final int tagCapacity) {
		//this(modeler.getDimension(), modeler.countEbd(), capacity);
		nodes = new ArrayList<JerboaDart>(capacity);
		this.modeler = modeler;
		this.capacity = capacity;
		this.dimension = modeler.getDimension();
		this.length = 0;
		modeler.countEbd();
		deletePool = null;
		tagManager = (new TagManagerFactory()).create(tagCapacity);
		intMarkers = new ArrayList<>();
		extMarkers = new ArrayList<>();
		idIntMarkers = new BitSet(Long.SIZE);
		idExtMarkers = Long.SIZE + 1;
		
		clear();
	}

	/**
	 * Getter of the current capacity of the gmap.
	 * @return Return the current capacity.
	 */
	public int getCapacity() {
		return capacity;
	}

	/**
	 * Getter of the current dimension of the gmap.
	 * @return The current dimension.
	 */
	public int getDimension() {
		return dimension;
	}

	/**
	 * This function check if the index correspond to an existent node. The node must not been marked as
	 * deleted.
	 *
	 * <b>Complicity: constant</b>
	 *
	 * @param i searched index of the node
	 * @return Return true if the index of the node exist and false otherwise.
	 */
	public boolean existNode(int i) {
		return (i < length) && !nodes.get(i).isDeleted();//(!deletePool.contains(nodes.get(i)));
	}


	/**
	 * This function returns the node with the index i. In the case of attempts to non existent node,
	 * this function returns the null pointer.
	 *
	 * <b>Complexity: constant</b>
	 * @param i index of the searched node
	 * @return Return the node at the position i or null otherwise.
	 */
	public JerboaDart getNode(int i) {
		if(existNode(i))
			return nodes.get(i);
		else
			return null;
	}

	/**
	 * This function is called internally for extending the current capacity of the gmap.
	 * And makes earlier allocation.
	 */
	private final void extendCapacity() {
		synchronized(this) {
			int newcapacity = (capacity<<1|1);
			nodes.ensureCapacity(newcapacity);
			for(int i=capacity;i < newcapacity; i++) {
				nodes.add(new JerboaDart(this, i));
			}
			capacity = newcapacity;
		}
	}

	/**
	 * This function add a new node in the current gmap and the return the created {@see JerboaNode}.
	 * If the gmap reaches the capacity, this method extends automatically the capacity.
	 * The complexity depends of the current state of the gmap, the most of time it will be constant,
	 * but in case of extension this method could take more time.
	 * @return Return the created node.
	 */
	public final JerboaDart addNode() {
		synchronized(this) {
			int size;
			if((size = deletePool.size()) > 0) {
				JerboaDart n = deletePool.remove(size-1);// pop();
				n.setDelete(false);
				return n;
			}
			if(length >= capacity) {
				extendCapacity();
			}
			return nodes.get(length++);
		}
	}


	public final JerboaDart[] addNodes(int size) {
		synchronized(this) {
			JerboaDart[] res = new JerboaDart[size];
			while(length+size >= capacity)
				extendCapacity();
			int p = 0;
			while(p < size && deletePool.size() > 0) {
				res[p] = deletePool.remove(size); // pop();
				res[p].setDelete(false);
				p++;
			}
			while(p < size) {
				res[p++] = nodes.get(length++);
			}
			return res;
		}
	}

	/**
	 * This function deletes the node of index i.
	 * The complexity is constant.
	 *
	 * @param i index of the node to be deleted.
	 */
	// TODO ici il manque peut etre une securite dans les operations
	// quand un etudiant veut faire n'importe quoi...
	public final void delNode(int n) {
		synchronized(this) {
			// presque aucune
			if(n < 0 || length <= n)
				return;
			JerboaDart node = nodes.get(n);
			if(node.isDeleted())
				return;
			cutNode(n);
			deletePool.add(node);
			node.setDelete(true);
		}
	}

	public final void makeDelete(int n) {
		if(n != length-1) {
			JerboaDart del = nodes.get(n);
			JerboaDart sub = nodes.get(length-1);
			del.switchEbd(sub);
			System.out.println("DEL: "+del.toString());
			System.out.println("SUB: "+sub.toString());
			for(int i=0;i <= dimension;i++) {
				JerboaDart oldvoisin = del.alpha(i);
				oldvoisin.setAlpha(i, oldvoisin);
				if(sub.alpha(i) != sub) {
					del.setAlpha(i, sub.alpha(i));
					sub.setAlpha(i, sub);
				}
				else {
					del.setAlpha(i, del);
				}
			}
			System.out.println("DEL: "+del.toString());
			System.out.println("SUB: "+sub.toString());
			System.out.println("======================================");
			sub.setDelete(true);
		}
		else {

			JerboaDart node = nodes.get(n);
			System.out.println("DEL: "+node.toString());
			for(int a=0;a <= dimension;a++) {
				JerboaDart tmp = node.alpha(a);
				System.out.println("1V"+a+": "+tmp.toString());
				tmp.setAlpha(a, tmp); // on met des boucles SUR LES DEUX
				node.setAlpha(a,node);
				System.out.println("2V"+a+": "+tmp.toString());
			}
			System.out.println("RES: "+node.toString());
			System.out.println("======================================");
		}
		length--;

	}

	/**
	 * This method is private and used exclusively inside the gmap, it optimizes the
	 * rule application in case you delete and creat new node at the same time.
	 * @param i
	 */
	private final void cutNode(int i) {
		JerboaDart node = nodes.get(i);
		for(int a=0;a <= dimension;a++) {
			JerboaDart tmp = node.alpha(a);
			tmp.setAlpha(a, tmp); // on met des boucles SUR LES DEUX
			node.setAlpha(a,node);
		}

	}

	// hyp: le noeud doit etre un noeud de la gmap en cours
	// on ne fait aucune verif a ce niveau
	public final void delNode(JerboaDart n) {
		delNode(n.getID());
	}

	/**
	 * This function make a set of checks in the current gmap:
	 * <ol>
	 * <li>check the coherence in the adjacent matrix</li>
	 * <li>check the coherence in the dimension of each node</li>
	 * <li>check the existence of neighbor for any existent node</li>
	 * <li>check the condition cycle</li>
	 * <li>check the coherence of node marked as deleted the delete pool</li>
	 * </ol>
	 * @return Return true and all checks are correct and false otherwise.
	 */
	public final boolean check(boolean checkEbd) {
		try {
			deepCheck(checkEbd);
			return true;
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/**
	 * @throws JerboaMalFormedException
	 * @throws JerboaException
	 */
	public final void deepCheck(boolean checkEbd) throws JerboaMalFormedException, JerboaException {
		// TODO: check on embedding (not allowed a NULL)
		// TODO: check the correct embedding in the orbit
		JerboaMalFormedException err = new JerboaMalFormedException();
		JerboaTracer.getCurrentTracer().setMinMax(0, length);
		JerboaTracer.getCurrentTracer().report("Check coherence adjacence...");
		System.out.println("LENGTH: "+length);
		for (int pos = 0; pos < length; pos++) {
			JerboaDart node = nodes.get(pos);
			if (node.getDimension() != dimension) {
				String msg = "["+node+"] The node has not the correct dimension "+dimension;
				err.put(node, msg);
				//System.out.println(msg);
				//throw new JerboaMalFormedException(node, msg);
				continue;
			}
			if (node.isDeleted()) {
				// on verif coherence avec le pool
				for (int i = 0; i <= dimension; i++) {
					if (node.alpha(i) != node) {
						String msg = "["+node+"] The node misses a loop for a"+i;
						err.put(node, msg);
					}
				}
			} else {
				for (int i = 0; i <= dimension; i++) {
					if (node.alpha(i) == null) {
						String msg = "The node has null pointer for a"+i;
						err.put(node, msg);
					}

					if (node.alpha(i).alpha(i) != node) {
						String msg = "Incoherent edge (a->b and b->a) for a"+i;
						err.put(node, msg);
					}
				}

				// condition de cycle
				for (int i = 0; i < dimension; i++) {
					for (int j = i + 2; j <= dimension; j++) {
						if (node.alpha(i).alpha(j).alpha(i).alpha(j) != node) {
							String msg = "Incorrect cycle with the a"+i+" a"+j;
							err.put(node,msg);
						}
					}
				}
			}
			JerboaTracer.getCurrentTracer().progress(pos);
		}

		JerboaTracer.getCurrentTracer().setMinMax(length, capacity);
		JerboaTracer.getCurrentTracer().report("Check correct dimension...");
		for(int pos = length; pos < nodes.size();pos++) {
			JerboaDart node = nodes.get(pos);
			if (node.getDimension() != dimension) {
				String msg = "["+node+"] "+"The unexistent node has not the correct dimension "+dimension;
				err.put(node, msg);
				continue;
			}
			for (int i = 0; i <= dimension; i++) {
				if (node.alpha(i) != node) {
					String msg = "["+node+"] "+ "The unexistent node misses a loop for a"+i;
					err.put(node, msg);
				}
			}
			JerboaTracer.getCurrentTracer().progress(pos);
		}

		int pos = 0;
		if (checkEbd) {
			JerboaTracer.getCurrentTracer().setMinMax(0, length);
			JerboaTracer.getCurrentTracer().report(
					"Check embedding in nodes...");
			List<JerboaEmbeddingInfo> ebdsinfo = modeler.getAllEmbedding();
			
			for (JerboaDart node : this) {
				for(JerboaEmbeddingInfo ebdinfo : ebdsinfo) {
					int i = ebdinfo.getID();
					Object ebd = node.embeddings[i];
					{
						JerboaOrbit orbit = ebdinfo.getOrbit();
						List<JerboaDart> set = orbit(node, orbit);
						for (JerboaDart n : set) {
							if (n.embeddings[i] != ebd) {
								String msg = "The embedding "
										+ ebdinfo.getName()+"("+ebdinfo.getID()+")"
										+ " is not correct in the associate orbit "
										+ orbit;
								err.put(node, msg);
							}
						}
						/*
						for (JerboaNode n2 : this) {
							if (!set.contains(n2)) {
								if (n2.embeddings[i] != null && ebd != null
										&& (n2.embeddings[i] == ebd)) { // patch:
									String msg = "The embedding " + ebdinfo.getName()+"("+i+")"
											+ " seems not unique for the orbit ("
											+ n2 + ")";
									err.put(node, msg);
								}
							}
						}
						*/
					}
				}
				JerboaTracer.getCurrentTracer().progress(pos++);
			}
			// throw new RuntimeException("Check Ebd NO MORE SUPPORTED!!!!");
		}

		JerboaTracer.getCurrentTracer().setMinMax(0, deletePool.size());
		JerboaTracer.getCurrentTracer().report("Check deleted nodes...");
		pos = 0;
		for (JerboaDart node : deletePool) {
			if (!node.isDeleted()) {
				String msg = "The node("+node.getID()+") is not marked as deleted!";
				err.put(node,msg);
			}
			JerboaTracer.getCurrentTracer().progress(pos++);
		}

		JerboaTracer.getCurrentTracer().done();

		if(err.isEmpty())
			System.out.println("[check] GMAP seems ok!");
		else
			throw err;
	}

	public JerboaDart node(int i) {
		return nodes.get(i);
	}

	public int getLength() {
		//return length - deletePool.size();
		return length;
	}

	public int size() {
		return length - deletePool.size();
	}

	

	/*
	 * sorb : orbite de plongement
	 * orb : orbite du parcours
	 */
	public List<JerboaDart> collect(final JerboaDart start,final JerboaOrbit orb, final JerboaOrbit sorb) throws JerboaException {
		JerboaMark markerOrb = creatFreeMarker(); 
		JerboaMark markerSorb = creatFreeMarker(); 
		
		try {
			return collectCustomMark(start, orb, sorb, markerOrb, markerSorb);
		}
		finally {
			freeMarker(markerOrb);
			freeMarker(markerSorb);
		}
	}
	
	public List<JerboaDart> collectCustomMark(final JerboaDart start,final JerboaOrbit orb, final JerboaOrbit sorb, final JerboaMark markerOrb, final JerboaMark markerSorb) throws JerboaException {
		// Check
		if(orb.getMaxDim() > dimension)
			throw new JerboaOrbitIncompatibleException("In orbit find a dimension "+orb.getMaxDim()+" whereas max accepted is "+dimension);
		if(sorb.getMaxDim() > dimension)
			throw new JerboaOrbitIncompatibleException("In sorbit find a dimension "+orb.getMaxDim()+" whereas max accepted is "+dimension);

		// Optimisation fact.
		JerboaOrbit so = sorb.simplify(orb);

		// declaration of stuff
		ArrayList<JerboaDart> res = new ArrayList<JerboaDart>();
		ArrayDeque<JerboaDart> stack = new ArrayDeque<JerboaDart>();
		stack.push(start);

		// double-scan of the orbits
		while(!stack.isEmpty()) {
			JerboaDart cur = stack.pop();
			if(cur.isNotMarked(markerOrb)) {
				mark(markerOrb,cur);
				// on va verifier si notre noeud n'est pas
				// deja reserve
				if(cur.isNotMarked(markerSorb)) {
					markOrbit(cur, so, markerSorb);
					//mark(markerSorb,cur);
					res.add(cur);
					// maintenant on va marquer tous les noeuds du sous-orbite
					// pour eviter de les reprendre
					//for(int a: so.tab()) {
					//	mark(markerSorb,cur.alpha(a));
					//}
				}
				for(int a : orb.tab()) {
					stack.push(cur.alpha(a));
				}
			}
		}		
		return res;
	}

	public <T> List<T> collect(final JerboaDart start,final JerboaOrbit orb, final JerboaOrbit sorb, final String ebdname) throws JerboaException {
		List<JerboaEmbeddingInfo> infos = modeler.getAllEmbedding();

		for (JerboaEmbeddingInfo info : infos) {
			if(info.getName().equals(ebdname)) {
				return collect(start, orb, sorb, info.getID());
			}
		}
		throw new JerboaRuntimeException("Unfound embedding name called: "+ebdname); 
	}

	public <T> List<T> collect(JerboaDart start, JerboaOrbit orb, JerboaOrbit sorb, int ebdID) throws JerboaException {
		ArrayList<T> ebds = new ArrayList<T>();

		List<JerboaDart> nodes = collect(start, orb, sorb);
		for (JerboaDart node : nodes) {
			ebds.add(node.<T>ebd(ebdID));
		}
		return ebds;
	}

	public <T> List<T> collect(JerboaDart start, JerboaOrbit orbit, int ebdID) throws JerboaException {
		ArrayList<T> ebds = new ArrayList<T>();
		JerboaOrbit ebdorbit = modeler.getEmbedding(ebdID).getOrbit();

		List<JerboaDart> nodes = collect(start, orbit, ebdorbit);
		for (JerboaDart node : nodes) {
			ebds.add(node.<T>ebd(ebdID));
		}
		return ebds;
	}

	public <T> List<T> collect(JerboaDart start, JerboaOrbit orbit, String ebdname) throws JerboaException {
		List<JerboaEmbeddingInfo> infos = modeler.getAllEmbedding();

		for (JerboaEmbeddingInfo info : infos) {
			if(info.getName().equals(ebdname)) {
				return collect(start, orbit, info.getID());
			}
		}
		throw new JerboaRuntimeException("Unfound embedding name called: "+ebdname);
	}

	public List<JerboaDart> orbit(JerboaDart start, JerboaOrbit orbit) throws JerboaException {
		JerboaMark markerOrb = creatFreeMarker();
		try {
			List<JerboaDart> res = markOrbit(start, orbit, markerOrb);
			return res;
		}
		finally {
			freeMarker(markerOrb);
		}
	}

	public List<JerboaDart> markOrbit(JerboaDart start, JerboaOrbit orbit, JerboaMark marker) throws JerboaException {
		if(orbit.getMaxDim() > dimension)
			throw new JerboaOrbitIncompatibleException("In orbit find a dimension "+orbit.getMaxDim()+" whereas max accepted is "+dimension);

		ArrayList<JerboaDart> res = new ArrayList<JerboaDart>();
		ArrayDeque<JerboaDart> stack = new ArrayDeque<JerboaDart>();
		stack.push(start);
		// TODO parallelisation du moteur
		// synchronized(vmarkers[marker]) 
		{
			while(!stack.isEmpty()) {
				JerboaDart cur = stack.pop();
				if(cur.isNotMarked(marker)) {
					mark(marker,cur);
					res.add(cur);
					for(int a : orbit.tab()) {
						stack.push(cur.alpha(a));
					}
				}
			}
		}
		return res;
	}
	
	@Override
	public List<JerboaDart> markOrbitException(JerboaDart start, JerboaOrbit orbit, JerboaMark marker, 
			HashMap<JerboaDart, Set<Integer>> exceptions)
		throws JerboaException {
		if(orbit.getMaxDim() > dimension)
			throw new JerboaOrbitIncompatibleException("In orbit find a dimension "+orbit.getMaxDim()+" whereas max accepted is "+dimension);

		ArrayList<JerboaDart> res = new ArrayList<JerboaDart>();
		ArrayDeque<JerboaDart> stack = new ArrayDeque<JerboaDart>();
		stack.push(start);
		//synchronized(vmarkers[marker]) 
		{
			while(!stack.isEmpty()) {
				JerboaDart cur = stack.pop();
				if(cur.isNotMarked(marker)) {
					mark(marker,cur);
					res.add(cur);
					if(exceptions.containsKey(cur)) {
						Set<Integer> modifs = exceptions.get(cur);
						for(int a : orbit.tab()) { 
							if(!modifs.contains(a))
								stack.push(cur.alpha(a));
						}
					}
					else {
						for(int a : orbit.tab()) {
							stack.push(cur.alpha(a));
						}
					}
				}
			}
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	public void clear() {
		nodes.clear();
		this.length = 0;
		for(int i=0;i < capacity; i++) {
			nodes.add(new JerboaDart(this, i));
		}
		deletePool = Collections.synchronizedList(new ArrayList<JerboaDart>(capacity>>3));
		for (JerboaMark jerboaMark : extMarkers) {
			try {
				jerboaMark.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		for (JerboaMark jerboaMark : intMarkers) {
			try {
				jerboaMark.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ArrayListG-Map dimension ")
		.append(dimension)
		.append(" load ")
		.append(length)
		.append("/")
		.append(capacity)
		.append(" without hole:")
		.append(deletePool.size())
		.append(" SIZE:").append(length-deletePool.size());

		return sb.toString();
	}


	class JerboaGMapIterator implements Iterator<JerboaDart> {
		int pos = 0;

		private void moveNext() {
			while(!existNode(pos) && pos < getLength()) pos++;
		}

		@Override
		public boolean hasNext() {
			moveNext();
			return pos < getLength();
		}

		@Override
		public JerboaDart next() {
			//for(int i=0;i < gmap.getLength();i++) {
			moveNext();
			JerboaDart node = getNode(pos);
			pos++;
			return node;
		}

		@Override
		public void remove() {
			// not supported
		}

	}

	@Override
	public Iterator<JerboaDart> iterator() {
		return new JerboaGMapIterator();
	}

	/**
	 * This function modifies the gmap by compacting all vertices of the gmap.
	 * Of course, this function does not modifies the topology.
	 */
	public void pack() {
		int size;
		while((size = deletePool.size()) > 0 ) {
			JerboaDart node = deletePool.remove(size-1);
			length--;
			JerboaDart last = nodes.get(length);
			int aid = node.getID();
			int bid = last.getID();
			Collections.swap(nodes, aid, bid);
			node.setID(bid);
			last.setID(aid);
			node.setDelete(false); // normalement inutile
			for (JerboaMark mark : extMarkers) {
				mark.swap(aid, bid);
			}
		}
	}
	
	/**
	 * This function modifies order or dart in the gmap. 
	 * But does not lose any topological relation or embedding).
	 * 
	 * Be careful! NO MARKER MUST BE <<IN USED>>!
	 */
	public void shuffle() {
		synchronized(this) {
			StopWatch sw = new StopWatch();
			sw.display("Start GMap shuffle...");
			pack();
			final int nbebds = modeler.countEbd();
			final int maxdim = modeler.getDimension();
			sw.display("...end pack.");
			Random rand = new Random();
			final int size = size();
			List<JerboaDart> freshdarts = new ArrayList<>(size);
			for(int i = 0; i < size; i++) {
				JerboaDart nd = new JerboaDart(this, i);
				freshdarts.add(nd);
			}
			ArrayList<JerboaDart> newnodes = new ArrayList<>(freshdarts);
			sw.display("...creation of darts.");
			Map<JerboaDart, JerboaDart> map = new HashMap<>();
			for (JerboaDart d : this) {
				int index = rand.nextInt(freshdarts.size());
				JerboaDart nd = freshdarts.remove(index);
				map.put(d, nd);
			}
			sw.display("...mapping indexing.");
			for(int i = 0; i < size; i++) {
				JerboaDart d = nodes.get(i);
				JerboaDart copyd = map.get(d);
				
				for(int dim = 0; dim <= maxdim; ++dim) {
					JerboaDart vd = d.alpha(dim);
					JerboaDart copyvd = map.get(vd);
					copyd.setAlpha(dim, copyvd);
				}
				
				for(int eid = 0; eid < nbebds; ++eid) {
					copyd.setEmbedding(eid, d.ebd(eid));
				}
			}
			sw.display("...cleaning old structures.");
			nodes.stream().forEach(d -> {
				for(int i = 0; i <= maxdim; ++i) {
					d.setAlpha(i, d);
					d.setDelete(true);
				}
			});
			nodes = newnodes;
			deletePool.clear();
			sw.display("end of shuffle.");
			
		}
	}
	
	public void swap(int aid, int bid) {
		if(0 <= aid && aid < length && 0 <= bid && bid < length) {
			JerboaDart a = nodes.get(aid);
			JerboaDart b = nodes.get(bid);
			Collections.swap(nodes, aid, bid);
			a.setID(bid);
			b.setID(aid);
			boolean ad = a.isDeleted();
			a.setDelete(b.isDeleted());
			b.setDelete(ad);
			for (JerboaMark mark : extMarkers) {
				mark.swap(aid, bid);
			}
		}
	}

	/**
	 * This function allows duplication of the current gmap into the argument.
	 * Caution, the criteria for allowing the duplication refers the same dimension
	 * between both GMap.
	 * Hint: you should pack the gmap before call this duplication, because holes are preserved
	 * in the inner structure.
	 *
	 * @param gmap: the gmap where making duplication
	 * @throws JerboaGMapDuplicateException: thrown when dimension mismatch
	 */
	public void duplicateInGMap(JerboaGMap gmap, JerboaGMapDuplicateFactory factory) throws JerboaGMapDuplicateException {
		if(gmap.getDimension() != dimension)
			throw new JerboaGMapDuplicateException("Mismatch dimension!");

		JerboaDart[] nnodes = gmap.addNodes(length);

		for(int i = 0;i < length;i++) {
			if(nodes.get(i).isDeleted()) {
				gmap.delNode(nnodes[i]);
			}
			else {
				for(int d=0; d <= dimension;d++) {
					int destid = nodes.get(i).alpha(d).getID();
					nnodes[i].setAlpha(d, nnodes[destid]);
				}
			}
		}

		List<JerboaEmbeddingInfo> ebds = modeler.getAllEmbedding();
		JerboaMark[] markers = new JerboaMark[ebds.size()];
		
		try {
			for(int i = 0;i < markers.length;i++)
				markers[i] = creatFreeMarker();

			System.out.println("LENGTH: "+length);
			for(int i = 0;i < length;i++) {
				JerboaDart oldnode = nodes.get(i);

				for (JerboaEmbeddingInfo info : ebds) {
					int ebdid = info.getID();
					if(oldnode.isNotMarked(markers[ebdid]) && !oldnode.isDeleted()) {
						if (factory.manageEmbedding(info)) {
							int id = info.getID();
							JerboaEmbeddingInfo new_info = factory.convert(info);
							int new_id = new_info.getID();
							List<JerboaDart> ns = markOrbit(oldnode, info.getOrbit(), markers[ebdid]);
							Object nvalue = factory.duplicate(info, oldnode.getEmbedding(id));
							for (JerboaDart n : ns) {
								nnodes[n.getID()].setEmbedding(new_id, nvalue);
							}
						}
					}
				}
			}
		} catch (JerboaNoFreeMarkException e) {
			e.printStackTrace();
		} catch (JerboaException e) {
			e.printStackTrace();
		}
		finally {
			for(int i = 0;i < markers.length;i++)
				freeMarker(markers[i]);
		}

	}

	@Override
	public void ensureCapacity(int v) {
		if(capacity >= v) 
			return;

		int newcapacity = capacity;
		while(newcapacity <= v)
			newcapacity = (newcapacity<<1|1);
		nodes.ensureCapacity(newcapacity);
		for(int i=capacity;i < newcapacity; i++) {
			nodes.add(new JerboaDart(this, i));
		}
		capacity = newcapacity;
	}

	@Override
	public JerboaModeler getModeler() {
		return modeler;
	}

	@Override
	public List<JerboaDart> getMarkedNode(JerboaMark marker) {
		// TODO FIXME HAK a refaire
		return null;
	}

	@Override
	public List<JerboaDart> unmarkOrbit(JerboaDart start, JerboaOrbit orbit,
			JerboaMark marker) throws JerboaException {
		if(orbit.getMaxDim() > dimension)
			throw new JerboaOrbitIncompatibleException("In orbit find a dimension "+orbit.getMaxDim()+" whereas max accepted is "+dimension);

		ArrayList<JerboaDart> res = new ArrayList<JerboaDart>();
		ArrayDeque<JerboaDart> stack = new ArrayDeque<JerboaDart>();
		stack.push(start);
		//synchronized(vmarkers[marker]) 
		{
			while(!stack.isEmpty()) {
				JerboaDart cur = stack.pop();
				if(cur.isMarked(marker)) {
					unmark(marker,cur);
					res.add(cur);
					for(int a : orbit.tab()) {
						stack.push(cur.alpha(a));
					}
				}
			}
		}
		return res;
	}

	@Override
	public void resetOrbitBuffer() {

	}

	@Override
	public boolean hasInvariant() {
		return false;
	}

	@Override
	public List<String> getInvariantNames() {
		return new ArrayList<>();
	}

	@Override
	public Stream<JerboaDart> stream() {
		return nodes.stream().filter(dart -> !dart.isDeleted() && dart.getID() < length);
	}
	
	@Override
	public Stream<JerboaDart> stream(boolean pack) {
		if(pack)
			return nodes.stream().filter(dart -> !dart.isDeleted() && dart.getID() < length);
		else
			return nodes.stream().map(dart -> { 
				if(!dart.isDeleted() && dart.getID() < length) 
					return dart;
				else
					return null;
			});
	}
	
	@Override
	public Stream<JerboaDart> parallelStream() {
		return nodes.parallelStream().filter(dart -> !dart.isDeleted() && dart.getID() < length);
	}
	
	@Override
	public Stream<JerboaDart> parallelStream(boolean pack) {
		if(pack)
			return nodes.parallelStream().filter(dart -> !dart.isDeleted() && dart.getID() < length);
		else 
			/*return IntStream.range(0, length).mapToObj(index -> {
				JerboaDart dart = nodes.get(index);
				if(dart != null && !dart.isDeleted()) 
					return dart;
				else
					return null;
			});*/
			return nodes.parallelStream().takeWhile(d -> d.getID() < length).map(dart -> { 
				if(!dart.isDeleted()) 
					return dart;
				else
					return null;
			});
	}
	


	@Override
	public int getTagCapacity() {
		return tagManager.getCapacity();
	}

	@Override
	public int getFreeTag() throws JerboaNoFreeTagException {
		return tagManager.take();
	}

	@Override
	public void freeTag(int index) {
		tagManager.free(index);
	}

	@Override
	public synchronized JerboaMark creatFreeMarker() {
		final int index  = idIntMarkers.previousClearBit(Long.SIZE - 1);
		if(index >= 0) {
			// System.err.println("internal marker");
			JerboaMarkBitwise marker = new JerboaMarkBitwise(this, index);
			intMarkers.add(marker);
			idIntMarkers.set(index);
			return marker;
		}
		else {
			// System.err.println("external marker");
			final int id = idExtMarkers++;
			// JerboaMark marker = new JerboaMarkBitSet(this, id);
			JerboaMark marker = new JerboaMarkBalancedTree(this, id);
			// JerboaMark marker = new JerboaMarkHashSet(this, id);
			extMarkers.add(marker);
			return marker;
		}
	}
	
	@Override
	public synchronized JerboaMark creatFreeMarker(boolean threadSafe) {
		// System.err.println("external marker");
		final int id = idExtMarkers++;
		// JerboaMark marker = new JerboaMarkBitSet(this, id);
		JerboaMark marker = new JerboaMarkBalancedTree(this, id);
		// JerboaMark marker = new JerboaMarkHashSet(this, id);
		extMarkers.add(marker);
		return marker;

	}

	@Override
	public synchronized void freeMarker(JerboaMark marker) {
		try {
			marker.close();
			if(marker.getID() < Long.SIZE) {
				marker.reset();
				idIntMarkers.clear(marker.getID());
				intMarkers.remove(marker);
			}
			else
				extMarkers.remove(marker);
		} catch (Exception e) {
			throw new JerboaRuntimeException(e);
		}
	}

	@Override
	public void mark(JerboaMark marker, JerboaDart cur) {
		assert marker.getGMap() == this;
		marker.mark(cur);
	}

	@Override
	public void unmark(JerboaMark marker, JerboaDart cur) {
		assert marker.getGMap() == this;
		marker.unmark(cur);
	}

	@Override
	public <T> void addDynEmbedding(JerboaEmbeddingInfo dynEbd, T initValue) {
		for (JerboaDart dart : nodes) {
			dart.setDynEmbedding(dynEbd.getName(), initValue);
		}
	}

	@Override
	public void delDynEmbedding(JerboaEmbeddingInfo dynEbd) {
		for(JerboaDart dart : nodes) {
			dart.delDynEmbedding(dynEbd);
		}
	}

}
