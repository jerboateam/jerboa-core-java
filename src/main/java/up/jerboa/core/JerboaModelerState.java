package up.jerboa.core;

import java.util.ArrayList;

public abstract class JerboaModelerState {
	private ArrayList<String> autorizedRules;
	private String name;
	
	private JerboaModeler modeler;
	
	public static JerboaModelerState any = new JerboaModelerState(null, "any") {
		@Override
		public boolean checkInvariant() {
			return true;
		}
	};
	
	
	public JerboaModelerState(JerboaModeler modeler, String name, String ...autorizedRules) {
		this.modeler = modeler;
		this.name = name;
		this.autorizedRules = new ArrayList<>();
		
		for (String rule : autorizedRules) {
			this.autorizedRules.add(rule);
		}
	}
	
	public String getName() { return name; }
	public JerboaModeler getModeler() { return modeler; }
	
	public boolean isAuthorized(String rulename) {
		return autorizedRules.contains(rulename) || (autorizedRules.size() == 0); 
	}
	
	
	public abstract boolean checkInvariant();
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("state<");
		sb.append(name).append("> {");
		if(autorizedRules.size() == 0)
			sb.append("*");
		else {
			for (String rule : autorizedRules) {
				sb.append(rule).append(";");
			}
		}
		sb.append("}");
		return sb.toString();	
	}
}
