package up.jerboa.core;

public class JerboaModelerStateDelegate extends JerboaModelerState {

	public interface JMSDelegate {
		public boolean checkInvariant(JerboaModelerState state);
	}

	private JMSDelegate delegate;
	
	public JerboaModelerStateDelegate(JerboaModeler modeler, String name, JMSDelegate delegate,String... autorizedRules) {
		super(modeler, name, autorizedRules);
		this.delegate = delegate;
	}

	@Override
	public boolean checkInvariant() {
		if(delegate == null)
			return true; // petit glitch pour dire qu'on accepte tout
		else
			return delegate.checkInvariant(this);
	}

}
