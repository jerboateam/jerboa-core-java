package up.jerboa.core.mark;

import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaMark;

public interface JerboaExternalMarkBuilder {
	JerboaMark creat(JerboaGMap gmap, int id);
}
