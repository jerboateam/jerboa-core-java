package up.jerboa.core.mark;

import java.util.HashSet;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaMark;

public class JerboaMarkBalancedTree implements JerboaMark {

	private JerboaGMap gmap;
	private int id;
	private HashSet<JerboaDart> darts;
	
	public JerboaMarkBalancedTree(JerboaGMap gmap, int id) {
		this.gmap = gmap;
		this.id = id;
		darts = new HashSet<>();
	}

	@Override
	public boolean isMarked(JerboaDart dart) {
		return darts.contains(dart);
	}

	@Override
	public boolean isNotMarked(JerboaDart dart) {
		return ! darts.contains(dart);
	}

	@Override
	public void mark(JerboaDart dart) {
		darts.add(dart);
	}

	@Override
	public void mark(JerboaDart... darts) {
		for (JerboaDart d : darts) {
			this.darts.add(d);
		}
	}

	@Override
	public void unmark(JerboaDart dart) {
		darts.remove(dart);
	}

	@Override
	public void unmark(JerboaDart... darts) {
		for (JerboaDart d : darts) {
			this.darts.remove(d);
		}
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public JerboaGMap getGMap() {
		return gmap;
	}

	@Override
	public void swap(int id1, int id2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void reset() {
		this.darts.clear();
	}

	@Override
	public void close() {
		reset();
	}

}
