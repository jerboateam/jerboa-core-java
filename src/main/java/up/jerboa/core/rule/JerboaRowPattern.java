/**
 * 
 */
package up.jerboa.core.rule;

import up.jerboa.core.JerboaDart;

/**
 * @author Hakim Belhaouari
 *
 */
public class JerboaRowPattern {

	private int columns;
	private int length;
	private JerboaDart nodes[];
	/**
	 * 
	 */
	public JerboaRowPattern(int columns) {
		this.columns = columns;
		length = 0;
		nodes = new JerboaDart[columns];
	}
	
	/**
	 * Constructs a new JerboaRowPattern that contains a single column and a single dart.
	 * 
	 * @param dart Contained dart.
	 */
	public JerboaRowPattern(JerboaDart dart) {
		columns = 1;
		length = 1;
		nodes = new JerboaDart[] {dart};
	}
	
	public boolean isFull() {
		return (length == columns);
	}
	
	public void setNode(int col, JerboaDart node) {
		if(nodes[col] == null)
			length++;
		nodes[col] = node;
	}
	
	public JerboaDart getNode(int col) {
		return nodes[col];
	}
	
	public JerboaDart get(int col) {
		return nodes[col];
	}
		
	public int size() {
		return columns;
	}
	
	public void clear() {
		for(int i=0;i < columns;i++) {
			nodes[i] = null;
		}
		length=0;
	}
	
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (JerboaDart node : nodes) {
			if(node != null)
				sb.append(" | ").append(node.getID());
			else
				sb.append(" | _");
		}		
		sb.append(" | ");
		return sb.toString();
	}
}
