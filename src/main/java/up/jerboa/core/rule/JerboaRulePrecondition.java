/**
 * 
 */
package up.jerboa.core.rule;

import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaRuleAtomic;

/**
 * @author hakim
 *
 */
public interface JerboaRulePrecondition {

	public boolean eval(JerboaGMap map, JerboaRuleAtomic rule);
}
