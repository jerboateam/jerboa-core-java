package up.jerboa.core.rule.engine.parallel;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaInputHooks;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.core.JerboaRuleAtomic;
import up.jerboa.core.JerboaRuleResult;
import up.jerboa.core.rule.JerboaRowPattern;
import up.jerboa.core.rule.JerboaRuleEngineAbstract;
import up.jerboa.core.rule.JerboaRuleExpression;
import up.jerboa.core.rule.JerboaRuleNode;
import up.jerboa.core.rule.engine.parallel.util.Islands;
import up.jerboa.core.rule.engine.parallel.util.Uniques;
import up.jerboa.exception.JerboaException;

public class JerboaAugmentationRuleParallelEngineBKP extends JerboaRuleEngineAbstract {

	/**
	 * Topological update task in the right adjacency matrix for a node.
	 * 
	 * A topological update is for a node either from an explicit alpha relation or an implicit alpha relation. 
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class TopologyTask {
		
		protected int id;
		
		public TopologyTask(int id) {
			this.id = id;
		}
		
		public abstract void connect(int[] leftMatrix, int[] rightMatrix, int k, int m);
		
	}
	
	/**
	 * Topological update task in the right adjacency matrix from an explicit alpha relation for a node.
	 * 
	 * This task only requires offsets between two nodes linked by explicit alpha relations.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ExplicitTopologyTask extends TopologyTask {
		
		private int[] alphas;
		private int[] offsets;
			
		public ExplicitTopologyTask(int id, int[] alphas, int[] offsets) {
			super(id);
			this.alphas = alphas;
			this.offsets = offsets;
		}

		@Override
		public void connect(int[] leftMatrix, int[] rightMatrix, int k, int m) {
			
			int begin = id * k;
			int end = begin + k;
			
			int[] offsets = Arrays.copyOf(this.offsets, this.offsets.length);
			for (int i = 0; i < offsets.length; ++i) offsets[i] *= k;
						
			IntStream.range(begin, end).parallel().forEach(tid -> {
				for (int i = 0; i < alphas.length; ++i)	
					rightMatrix[tid * m + alphas[i]] = tid + offsets[i];
			});
		}
		
	}
	
	/**
	 * Topological update task in the right adjacency matrix from an implicit alpha relation for node.
	 * 
	 * This task only requires an offset of the treated node in the left adjacency matrix.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ImplicitTopologyTask extends TopologyTask  {
		
		private int[] leftAlphas;
		private int[] rightAlphas;
		
		public ImplicitTopologyTask(int id, int[] leftAlphas, int[] rightAlphas) {
			super(id);
			this.leftAlphas = leftAlphas;
			this.rightAlphas = rightAlphas;
		}

		@Override
		public void connect(int[] leftMatrix, int[] rightMatrix, int k, int m) {
			
			int begin = id * k;
			int end = begin + k;
			
			IntStream.range(begin, end).parallel().forEach(tid -> {
				for (int i = 0; i < leftAlphas.length; ++i)
					rightMatrix[tid * m + rightAlphas[i]] = leftMatrix[(tid % k) * m + leftAlphas[i]] + begin;
			});			
		}
		
	}
	
	/**
	 * Expressions that are not held by nodes attached to the single left node.
	 * 
	 * Those expressions can be computed and spread in a single stage because 
	 * their applications do not modify the left pattern.
	 * 
	 * Also those expressions can only be included in the right nodes holding them because 
	 * those nodes are created nodes and their topological relations are fully specified by the rule.
	 * If their topological relations were not, the rules would have not been correct.
	 * 
	 * Those expressions can be hold by one or many right nodes.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class FreeExpressionTask {
		
		public abstract void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m);
		
	}
	
	/**
	 * Expressions that are held by a single right node that is not attached to the single left node.
	 * 
	 * The node holding the expression is not attached to the left node so its topology is fully known.
	 * The expression orbit is either equivalent or included in the node orbit.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class SingleFreeExpressionTask extends FreeExpressionTask {
		
	}
	
	/**
	 * Expressions that are held by a single right node that is not attached to the single left node.
	 * The expression orbit is strictly included in the node orbit.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * TR and TF must be computed.
	 * TF is used to compute new embedding values.
	 * TR is used to spread new embedding values.
	 * 
	 * For example Menger rules has :
	 * - n2 with orbit <0,_,2,3>
	 * - embedding orbit <0,2,3>
	 * <0,2,3> is strictly included is <0,_,2,3> because of '_'.
	 * 
	 * @author Pierre Bourquat
	 */
	private class IncludedSingleFreeExpressionTask extends SingleFreeExpressionTask {

	    private JerboaRuleExpression expression;
	    private JerboaOrbit orbit;
	    
	    private JerboaRuleNode node;
		
		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {
			
			int begin = node.getID() * k;
			int end = begin + k;
			
			int[] TR = Islands.island1D(matrix, m, begin, end, orbit.tab());
			int[] TF = Uniques.byBitSet(TR, begin, end);
			
			int index = expression.getEmbedding();
			
			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				try {
					int tf = TF[tid];
					Object ebd = expression.compute(gmap, owner, leftPattern.get(tf % k), node);
					darts.get(tf).setEmbedding(index, ebd);
				} catch (JerboaException e) {
	                e.printStackTrace();
				}
			});
			
			IntStream.range(0, TR.length).parallel().forEach(tid -> {
				int id = begin + tid;
				int tr = TR[tid];
				if (id != tr) darts.get(id).setEmbedding(index, darts.get(tr).getEmbedding(index));
			});
		}

	}
	
	/**
	 * Expressions that are held by a single right node that is not attached to the single left node.
	 * The expression orbit is strictly equivalent to the node orbit.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * This case may only occur if a copy of the left pattern is done.
	 * 
	 * @author Pierre Bourquat
	 */
	private class EquivalentSingleFreeExpressionTask extends SingleFreeExpressionTask {
		
	    private JerboaRuleExpression expression;
	    
	    private JerboaRuleNode node;
		
		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {
			try {
				
				int begin = node.getID() * k;
				int end = begin + k;
				
				Object ebd = expression.compute(gmap, owner, leftPattern.get(begin), node);
				
				int index = expression.getEmbedding();
				IntStream.range(begin, end).parallel().forEach(tid -> {
					darts.get(tid).setEmbedding(index, ebd);
				});
				
			} catch (JerboaException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is either equivalent or included in the orbit formed by the holding nodes.
	 * 
	 * The concerned nodes are either contiguous or sparse.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class ManyFreeExpressionTask extends FreeExpressionTask {
		
	}
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is included in the orbit formed by the holding nodes.
	 * 
	 * The concerned nodes are either contiguous or sparse.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * TR and TF must be computed.
	 * TF is used to compute new embedding values.
	 * TR is used to spread new embedding values.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class IncludedManyFreeExpressionTask extends ManyFreeExpressionTask {
		
	}	
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is included in the orbit formed by the holding nodes.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * TR and TF must be computed.
	 * TF is used to compute new embedding values.
	 * TR is used to spread new embedding values.
	 * 
	 * TR can be computed in a single contiguous block because concerned nodes are contiguous.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ContiguousIncludedManyFreeExpressionTask extends IncludedManyFreeExpressionTask {
			
	    private JerboaRuleExpression expression;
	    private JerboaOrbit orbit;
	    
	    private JerboaRuleNode node;
	    
	    private int begin;
	    private int end;
	    
		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {

			int begin = this.begin * k;
			int end = this.end * k;
			
			int[] TR = Islands.island1D(matrix, m, begin, end, orbit.tab());
			int[] TF = Uniques.byBitSet(TR, begin, end);
			
			int index = expression.getEmbedding();
			
			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				try {
					int tf = TF[tid];
					Object ebd = expression.compute(gmap, owner, leftPattern.get(tf % k), node);
					darts.get(tf).setEmbedding(index, ebd);
				} catch (JerboaException e) {
	                e.printStackTrace();
				}
			});
			
			IntStream.range(0, TR.length).parallel().forEach(tid -> {
				int id = begin + tid;
				int tr = TR[tid];
				if (id != tr) darts.get(id).setEmbedding(index, darts.get(tr).getEmbedding(index));
			});
		}
		
	}
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is included in the orbit formed by the holding nodes.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * TR and TF must be computed.
	 * TF is used to compute new embedding values.
	 * TR is used to spread new embedding values.
	 * 
	 * TR is computed with a sparse block because concerned nodes are sparse.
	 * 
	 * @author Pierre Bourquat
	 */
	private class SparseIncludedManyFreeExpressionTask extends IncludedManyFreeExpressionTask {
		
	    private JerboaRuleExpression expression;
	    private JerboaOrbit orbit;
	    
	    private JerboaRuleNode node;
	    
	    private int[][] strides;
		
		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {
			
			int[][] strides = new int[this.strides.length][2];
			for (int i = 0; i < this.strides.length; ++i) {
				strides[i][0] = this.strides[i][0] * k;
				strides[i][1] = this.strides[i][1] * k;
			}
			
		}
		
	}	
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is equivalent to the orbit formed by the holding nodes.
	 * 
	 * The concerned nodes are either contiguous or sparse.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class EquivalentManyFreeExpressionTask extends ManyFreeExpressionTask {
		
	}	
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is equivalent to the orbit formed by the holding nodes.
	 * 
	 * The concerned nodes are either contiguous.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * Spread operation can be done with a single contiguous block because concerned nodes are contiguous.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ContiguousEquivalentManyFreeExpressionTask extends EquivalentManyFreeExpressionTask {

		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {
			// TODO Auto-generated method stub
			
		}
		
	}	
	
	/**
	 * Expressions held by many right nodes not attached to the left node.
	 * 
	 * Because their are not attached to the left node, their topological are fully known.
	 * The expression orbit is equivalent to the orbit formed by the holding nodes.
	 * 
	 * The concerned nodes are either contiguous.
	 * 
	 * Compute and spread operation can be done in a single stage.
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * Spread operation is done with a sparse block because concerned nodes are sparse.
	 * 
	 * @author Pierre Bourquat
	 */
	private class SparseEquivalentManyFreeExpressionTask extends EquivalentManyFreeExpressionTask {

		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {
			// TODO Auto-generated method stub
			
		}
		
	}	
	
	/**
	 * Expressions that are held by nodes attached to the single left node.
	 * 
	 * Those expressions can not be computed and spread in a single stage because 
	 * their applications will modify the left pattern.
	 * First stage computes the new embedding values.
	 * Second stage spreads them.
	 * 
	 * Those expressions can be hold by one or many right nodes.
	 * 
	 * If the expression concerns only one node, it is the single left.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class HookedExpressionTask {
		
		public abstract void compute();
		public abstract void speard();
		
	}
	
	/**
	 * Expressions that are held by the left node (in the right nodes).
	 * These expressions tasks are similar to the expression tasks of JerboaEngineUpdateParallel.
	 * 
	 * An expression orbit is either strictly included, equivalent or partially included in the holding node orbit. 
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class SingleHookedExpressionTask extends HookedExpressionTask {
		
	}
	
	/**
	 * Expressions that are held by the left node (in the right nodes).
	 * Expression orbits are stricly included in the holding node orbit.
	 * 
	 * TR and TF must be computed and stored for the spread stage.
	 * TF is used to compute new embeddings and TR is used to spread new embeddings.
	 * 
	 * @author Pierre Bourquat
	 */
	private class IncludedSingleHookedExpressionTask extends SingleHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Expressions that are held by the left node (in the right nodes).
	 * Expression orbits is equivalent to the holding node orbit.
	 * 
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * @author Pierre Bourquat
	 */
	private class EquivalentSingleHookedExpressionTask extends SingleHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Expressions that are held by the left node (in the right nodes).
	 * Expression orbits is partially included in the holding node orbit.
	 * 
	 * TR and TF must be computed and only TF must be stored to spread the new embeddings.
	 * TF is used to compute new embeddings.
	 * Spreading operation is done by doing a classic orbit for each representative in TF.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ExcludedSingleHookedExpressionTask extends SingleHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * 
	 * Even the holding nodes are attached to the left node, their topological are fully known because 
	 * their are created nodes.
	 * The expression orbit is either equivalent or included in the orbit formed by the holding nodes.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class ManyHookedExpressionTask extends HookedExpressionTask {
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * The expression orbit is strictly included in the orbit formed by the holding nodes.
	 * 
	 * TR and TF must be computed and stored for the spread stage.
	 * TF is used to compute new embeddings and TR is used to spread new embeddings.
	 * 
	 * The holding nodes are either contiguous or sparse.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class IncludedManyHookedExpressionTask extends ManyHookedExpressionTask {
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * The expression orbit is strictly included in the orbit formed by the holding nodes.
	 * 
	 * TR and TF must be computed and stored for the spread stage.
	 * TF is used to compute new embeddings and TR is used to spread new embeddings.
	 * 
	 * The holding nodes are contiguous.
	 * Spread operation can be done with a single contiguous block.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ContiguousIncludedManyHookedExpressionTask extends IncludedManyHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * The expression orbit is strictly included in the orbit formed by the holding nodes.
	 * 
	 * TR and TF must be computed and stored for the spread stage.
	 * TF is used to compute new embeddings and TR is used to spread new embeddings.
	 * 
	 * The holding nodes are sparse.
	 * Spread operation is done with a sparse block.
	 * 
	 * @author Pierre Bourquat
	 */
	private class SparseIncludedManyHookedExpressionTask extends IncludedManyHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * The expression orbit is equivalent to the orbit formed by the holding nodes.
	 * 
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * The holding nodes are either contiguous or sparse.
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class EquivalentManyHookedExpressionTask extends ManyHookedExpressionTask {
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * The expression orbit is equivalent to the orbit formed by the holding nodes.
	 * 
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * The holding nodes are either contiguous.
	 * Spread operation can be done with a single contiguous block.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ContiguousEquivalentManyHookedExpressionTask extends EquivalentManyHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	/**
	 * Expressions that are held by many nodes attached to the left node.
	 * The expression orbit is equivalent to the orbit formed by the holding nodes.
	 * 
	 * Their is no need compute TR or TF because their is only one representative dart
	 * and any darts can be used as representative.
	 * 
	 * The holding nodes are sparse.
	 * Spread operation is done with a sparse block.
	 * 
	 * @author Pierre Bourquat
	 */
	private class SparseEquivalentManyHookedExpressionTask extends EquivalentManyHookedExpressionTask {

		@Override
		public void compute() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void speard() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private transient List<JerboaRowPattern> leftPattern;
	
	public JerboaAugmentationRuleParallelEngineBKP(JerboaRuleAtomic owner) {
		super(owner, JerboaAugmentationRuleParallelEngineBKP.class.getName());
		// TODO Auto-generated constructor stub
	}

	@Override
	public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaInputHooks hooks) throws JerboaException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<JerboaRowPattern> getLeftPattern() {
		return leftPattern;
	}

	@Override
	public int countCorrectLeftRow() {
		return 0;
	}
	
}
