package up.jerboa.core.rule.engine.parallel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaInputHooks;
import up.jerboa.core.JerboaMark;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.core.JerboaRuleAtomic;
import up.jerboa.core.JerboaRuleResult;
import up.jerboa.core.rule.JerboaNodePrecondition;
import up.jerboa.core.rule.JerboaRowPattern;
import up.jerboa.core.rule.JerboaRuleEngineAbstract;
import up.jerboa.core.rule.JerboaRuleExpression;
import up.jerboa.core.rule.JerboaRuleNode;
import up.jerboa.core.rule.engine.parallel.util.Adjacencies;
import up.jerboa.core.rule.engine.parallel.util.Islands;
import up.jerboa.core.rule.engine.parallel.util.Uniques;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaRuleAppFoldException;
import up.jerboa.exception.JerboaRuleApplicationException;
import up.jerboa.exception.JerboaRuleEngineException;
import up.jerboa.exception.JerboaRulePreconditionFailsException;
import up.jerboa.util.StopWatch;

public class JerboaRuleEngineAugmentationParallelV1 extends JerboaRuleEngineAbstract {
	
	public static boolean DEBUG = true;
	private static final StopWatch DEBUG_CHRONO = new StopWatch();

	private class FilterTask {

		private int[] loops;

		public FilterTask(int[] alphas) {
			this.loops = alphas;
		}

		public Optional<JerboaDart> filter(List<JerboaDart> darts) {
			return darts.parallelStream().filter(dart -> {
				for (int alpha : loops) {
					if (dart.alpha(alpha).getID() != dart.getID()) return true;
				}
				return false;
			}).findAny();
		}

		@Override
		public String toString() {
			return "loops: " + Arrays.toString(loops);
		}

	}

	/**
	 * Topological update task in the right adjacency matrix for a node.
	 * 
	 * A topological update is for a node either from an explicit alpha relation or an implicit alpha relation. 
	 * 
	 * @author Pierre Bourquat
	 */
	private abstract class AlphaTask {

		protected int id;

		public AlphaTask(int id) {
			this.id = id;
		}

		public abstract void connect(int[] leftMatrix, int[] rightMatrix, int k, int m);

	}

	/**
	 * Topological update task in the right adjacency matrix from an explicit alpha relation for a node.
	 * 
	 * This task only requires offsets between two nodes linked by explicit alpha relations.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ExplicitAlphaTask extends AlphaTask {

		private int[] alphas;
		private int[] offsets;

		public ExplicitAlphaTask(int id, int[] alphas, int[] offsets) {
			super(id);
			this.alphas = alphas;
			this.offsets = offsets;
		}

		@Override
		public void connect(int[] leftMatrix, int[] rightMatrix, int k, int m) {

			int begin = id * k;
			int end = begin + k;

			int[] offsets = new int[this.offsets.length];
			for (int i = 0; i < offsets.length; ++i) offsets[i] = this.offsets[i] * k;

			IntStream.range(begin, end).parallel().forEach(tid -> {
				for (int i = 0; i < alphas.length; ++i)	
					rightMatrix[tid * m + alphas[i]] = tid + offsets[i];
			});
		}

		@Override
		public String toString() {
			return "n" + id + ": explicit " + IntStream.range(0, alphas.length)
			.mapToObj(tid -> "n" + id + " -" + alphas[tid] + "-> n" + (id + offsets[tid]))
			.collect(Collectors.joining(", ", "[", "]"));
		}

	}

	/**
	 * Topological update task in the right adjacency matrix from an implicit alpha relation for node.
	 * 
	 * This task only requires an offset of the treated node in the left adjacency matrix.
	 * 
	 * @author Pierre Bourquat
	 */
	private class ImplicitAlphaTask extends AlphaTask {

		private int[] leftAlphas;
		private int[] rightAlphas;

		public ImplicitAlphaTask(int id, int[] leftAlphas, int[] rightAlphas) {
			super(id);
			this.leftAlphas = leftAlphas;
			this.rightAlphas = rightAlphas;
		}

		@Override
		public void connect(int[] leftMatrix, int[] rightMatrix, int k, int m) {

			int begin = id * k;
			int end = begin + k;

			IntStream.range(begin, end).parallel().forEach(tid -> {
				for (int i = 0; i < leftAlphas.length; ++i)
					rightMatrix[tid * m + rightAlphas[i]] = leftMatrix[(tid % k) * m + leftAlphas[i]] + begin;
			});			
		}

		@Override
		public String toString() {
			return "n" + id + ": implicit " + Arrays.toString(leftAlphas) + "->" + Arrays.toString(rightAlphas);
		}

	}

	private abstract class FreeExpressionTask {

		protected JerboaRuleExpression expression;
		protected JerboaRuleNode node;
		protected int[][] strides;

		public FreeExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides) {
			this.expression = expression;
			this.node = node;
			this.strides = strides;
		}

		public abstract void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m);

	}

	private class IncludedFreeExpressionTask extends FreeExpressionTask {

		private JerboaOrbit orbit;

		transient private boolean contiguous;

		public IncludedFreeExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides, JerboaOrbit orbit) {
			super(expression, node, strides);
			this.orbit = orbit;
			contiguous = strides.length == 1;
		}

		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {

			int[][] strides = new int[this.strides.length][2];
			for (int i = 0; i < this.strides.length; ++i) {
				strides[i][0] = this.strides[i][0] * k;
				strides[i][1] = this.strides[i][1] * k;
			}

			int[] TR = contiguous ? Islands.island1D(matrix, m, strides[0][0], strides[0][1], orbit.tab()) : Islands.island1D(matrix, m, strides, orbit.tab());
			int[] TF = Uniques.byBitSet(TR, strides[0][0], strides[this.strides.length - 1][1]);

			int index = expression.getEmbedding();

			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				try {
					int tf = TF[tid];
					Object ebd = expression.compute(gmap, owner, leftPattern.get(tf % k), node);
					darts.get(tf).setEmbedding(index, ebd);
				} catch (JerboaException e) {
					e.printStackTrace();
				}
			});

			int begin = strides[0][0];
			if (contiguous)	IntStream.range(0, TR.length).parallel().forEach(tid -> {
				int id = begin + tid;
				int tr = TR[tid];
				if (id != tr) darts.get(id).setEmbedding(index, darts.get(tr).getEmbedding(index));
			});
			else IntStream.range(0, TR.length).parallel().forEach(tid -> {
				int tr = TR[tid];
				if (tr != Islands.SKIPPED) {
					int id = begin + tid;
					if (id != tr) darts.get(id).setEmbedding(index, darts.get(tr).getEmbedding(index));
				}
			});
		}

		@Override
		public String toString() {
			return expression.getName() + ": included " + Arrays.deepToString(strides);
		}

	}

	private class EquivalentFreeExpressionTask extends FreeExpressionTask {

		public EquivalentFreeExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides) {
			super(expression, node, strides);
		}

		@Override
		public void computeAndSpread(JerboaGMap gmap, List<JerboaDart> darts, int[] matrix, int k, int m) {
			try {

				Object ebd = expression.compute(gmap, owner, leftPattern.get(0), node);

				int index = expression.getEmbedding();
				for (int i = 0; i < strides.length; ++i) {
					int[] row = strides[i];
					IntStream.range(row[0] * k, row[1] * k).parallel().forEach(tid -> {
						darts.get(tid).setEmbedding(index, ebd);
					});
				}

			} catch (JerboaException e) {
				e.printStackTrace();
			}
		}

		@Override
		public String toString() {
			return expression.getName() + ": equivalent " + Arrays.deepToString(strides);
		}

	}

	private abstract class HookedExpressionTask {

		protected JerboaRuleExpression expression;
		protected JerboaRuleNode node;
		protected int[][] strides;

		public HookedExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides) {
			super();
			this.expression = expression;
			this.node = node;
			this.strides = strides;
		}

		public abstract void compute(JerboaGMap gmap, int[] matrix, int k, int m);
		public abstract void speard(JerboaGMap gmap, List<JerboaDart> darts, int k);

	}

	private class IncludedHookedExpressionTask extends HookedExpressionTask {

		private JerboaOrbit orbit;

		transient private boolean contiguous;
		transient private int[] TR;
		transient private int[] TF;
		transient private Object[] ebds;

		public IncludedHookedExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides,
				JerboaOrbit orbit) {
			super(expression, node, strides);
			this.orbit = orbit;
			contiguous = strides.length == 1;
		}

		@Override
		public void compute(JerboaGMap gmap, int[] matrix, int k, int m) {

			int[][] strides = new int[this.strides.length][2];
			for (int i = 0; i < this.strides.length; ++i) {
				strides[i][0] = this.strides[i][0] * k;
				strides[i][1] = this.strides[i][1] * k;
			}

			if (contiguous) TR = Islands.island1D(matrix, m, strides[0][0], strides[0][1], orbit.tab());
			else TR = Islands.island1D(matrix, m, strides, orbit.tab());
			TF = Uniques.byBitSet(TR, strides[0][0], strides[this.strides.length - 1][1]);

			ebds = new Object[TF.length];
			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				try {
					ebds[tid] = expression.compute(gmap, owner, leftPattern.get(TF[tid] % k), node);
				} catch (JerboaException e) {
					e.printStackTrace();
				}
			});
		}

		@Override
		public void speard(JerboaGMap gmap, List<JerboaDart> darts, int k) {

			int index = expression.getEmbedding();

			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				darts.get(TF[tid]).setEmbedding(index, ebds[tid]);
			});

			int begin = strides[0][0] * k;
			if (contiguous)	IntStream.range(0, TR.length).parallel().forEach(tid -> {
				int id = begin + tid;
				int tr = TR[tid];
				if (id != tr) darts.get(id).setEmbedding(index, darts.get(tr).getEmbedding(index));
			});
			else IntStream.range(0, TR.length).parallel().forEach(tid -> {
				int tr = TR[tid];
				if (tr != Islands.SKIPPED) {
					int id = begin + tid;
					if (id != tr) darts.get(id).setEmbedding(index, darts.get(tr).getEmbedding(index));
				}
			});

			TR = null;
			TF = null;
			ebds = null;
		}

	}

	private class EquivalentHookedExpressionTask extends HookedExpressionTask {

		transient private Object ebd;

		public EquivalentHookedExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides) {
			super(expression, node, strides);
		}

		@Override
		public void compute(JerboaGMap gmap, int[] matrix, int k, int m) {
			try {
				ebd = expression.compute(gmap, owner, leftPattern.get(0), node);
			} catch (JerboaException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void speard(JerboaGMap gmap, List<JerboaDart> darts, int k) {

			int index = expression.getEmbedding();
			for (int i = 0; i < strides.length; ++i) {
				int[] row = strides[i];
				IntStream.range(row[0] * k, row[1] * k).parallel().forEach(tid -> {
					darts.get(tid).setEmbedding(index, ebd);
				});
			}

			ebd = null;
		}

	}

	private class ExcludedHookedExpressionTask extends HookedExpressionTask {

		private JerboaOrbit full;
		private JerboaOrbit include;

		transient private boolean contiguous;
		transient private int[] TF;
		transient private Object[] ebds;

		public ExcludedHookedExpressionTask(JerboaRuleExpression expression, JerboaRuleNode node, int[][] strides,
				JerboaOrbit full, JerboaOrbit include) {
			super(expression, node, strides);
			this.full = full;
			this.include = include;
			contiguous = strides.length == 1;
		}

		@Override
		public void compute(JerboaGMap gmap, int[] matrix, int k, int m) {

			int[][] strides = new int[this.strides.length][2];
			for (int i = 0; i < this.strides.length; ++i) {
				strides[i][0] = this.strides[i][0] * k;
				strides[i][1] = this.strides[i][1] * k;
			}

			int[] TR = contiguous ? Islands.island1D(matrix, m, strides[0][0], strides[0][1], include.tab()) : Islands.island1D(matrix, m, strides, include.tab());
			TF = Uniques.byBitSet(TR, 0, k);

			ebds = new Object[TF.length];
			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				try {
					ebds[tid] = expression.compute(gmap, owner, leftPattern.get(TF[tid] % k), node);
				} catch (JerboaException e) {
					e.printStackTrace();
				}
			});

		}

		@Override
		public void speard(JerboaGMap gmap, List<JerboaDart> darts, int k) {

			int n = Runtime.getRuntime().availableProcessors();
			JerboaMark[] markers = new JerboaMark[n];
			for (int i = 0; i < n; ++i) markers[i] = gmap.creatFreeMarker();

			int index = expression.getEmbedding();
			IntStream.range(0, TF.length).parallel().forEach(tid -> {
				Object ebd = ebds[tid];
				JerboaMark marker = markers[(int) Thread.currentThread().getId() % n];
				try {
					List<JerboaDart> spread = gmap.markOrbit(darts.get(TF[tid]), full, marker);
					for (JerboaDart dart : spread) dart.setEmbedding(index, ebd);
				} catch (JerboaException e) {
					e.printStackTrace();
				}
			});

			for (int i = 0; i < n; ++i) gmap.freeMarker(markers[i]);

			TF = null;
			ebds = null;
		}

	}

	private class CopyTask {

		private int index;
		private int[][] strides;

		public CopyTask(int index, int[][] strides) {
			this.index = index;
			this.strides = strides;
		}

		public void copy(List<JerboaDart> left, List<JerboaDart> right) {
			int k = left.size();
			for (int i = 0; i < strides.length; ++i)
				IntStream.range(strides[i][0] * k, strides[i][1] * k).parallel().forEach(tid -> {
					right.get(tid).setEmbedding(index, left.get(tid % k).getEmbedding(index));
				});			
		}

	}

	private FilterTask filterTask;
	private ArrayList<AlphaTask> alphaTasks;
	private ArrayList<HookedExpressionTask> hookedTasks;
	private ArrayList<FreeExpressionTask> freeTasks;
	private ArrayList<CopyTask> copyTasks; 

	private transient List<JerboaRowPattern> leftPattern;

	public JerboaRuleEngineAugmentationParallelV1(JerboaRuleAtomic owner) {
		super(owner, JerboaRuleEngineAugmentationParallelV1.class.getName());

		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		
		if (Runtime.getRuntime().availableProcessors() == 1) 
			throw new JerboaRuleEngineException("Only one PU is available.");

		if (owner.getLeftGraph().size() != 1) 
			throw new JerboaRuleEngineException("Left pattern must contain only one node.");
		if (owner.getRightGraph().size() < 2) 
			throw new JerboaRuleEngineException("Right pattern must contain at least two nodes.");

		alphaTasks = new ArrayList<>();
		hookedTasks = new ArrayList<>();
		freeTasks = new ArrayList<>();
		copyTasks = new ArrayList<>();

		int m = owner.getOwner().getDimension() + 1;
		JerboaRuleNode leftNode = owner.getLeftRuleNode(0);

		ArrayList<Integer> loops = new ArrayList<>();
		for (int alpha = 0; alpha < m; ++alpha)	if (Objects.nonNull(leftNode.alpha(alpha))) loops.add(alpha);
		if (!loops.isEmpty()) filterTask = new FilterTask(loops.stream().mapToInt(i -> i).toArray());

		boolean[][] computed = new boolean[owner.getOwner().getAllEmbedding().size()][owner.getRight().size()];

		for (JerboaRuleNode node : owner.getRight()) {

			ArrayList<int[]> pairs = new ArrayList<>();

			for (int alpha = 0; alpha < m; ++alpha) {
				JerboaRuleNode next = node.alpha(alpha);
				if (Objects.nonNull(next)) pairs.add(new int[] {alpha, next.getID() - node.getID()});
			}
			if (!pairs.isEmpty()) {

				int[] alphas = new int[pairs.size()];
				int[] offsets = new int[pairs.size()];

				IntStream.range(0, pairs.size()).forEach(tid -> {
					int[] pair = pairs.get(tid);
					alphas[tid] = pair[0];
					offsets[tid] = pair[1];
				});

				alphaTasks.add(new ExplicitAlphaTask(node.getID(), alphas, offsets));
			}

			pairs.clear();

			JerboaOrbit leftOrbit = leftNode.getOrbit();
			JerboaOrbit rightOrbit = node.getOrbit();
			for (int i = 0; i < leftOrbit.size(); ++i) {
				int right = rightOrbit.get(i);
				if (right == JerboaOrbit.NOVALUE) continue;
				pairs.add(new int[] {leftOrbit.get(i), right});
			}
			if (!pairs.isEmpty()) {

				int[] leftAlphas = new int[pairs.size()];
				int[] rightAlphas = new int[pairs.size()];

				IntStream.range(0, pairs.size()).forEach(tid -> {
					int[] pair = pairs.get(tid);
					leftAlphas[tid] = pair[0];
					rightAlphas[tid] = pair[1];
				});

				alphaTasks.add(new ImplicitAlphaTask(node.getID(), leftAlphas, rightAlphas));
			}

			for (JerboaRuleExpression expression : node.getExpressions()) {

				JerboaOrbit expressionOrbit = owner.getOwner().getEmbedding(expression.getEmbedding()).getOrbit();
				List<JerboaRuleNode> nodes = JerboaRuleNode.orbit(node, expressionOrbit);
				int[][] strides = slice(nodes);

				if (isHooked(nodes)) {
					if (isIncluded(expressionOrbit, nodes)) {
						if (isIncluded(nodes, expressionOrbit)) hookedTasks.add(new EquivalentHookedExpressionTask(expression, node, strides));
						else hookedTasks.add(new IncludedHookedExpressionTask(expression, node, strides, expressionOrbit));
					} else hookedTasks.add(new ExcludedHookedExpressionTask(expression, node, strides, expressionOrbit, simplify(nodes, expressionOrbit)));
				} else {
					if (isIncluded(expressionOrbit, nodes)) {
						if (isIncluded(nodes, expressionOrbit)) freeTasks.add(new EquivalentFreeExpressionTask(expression, node, strides));
						else freeTasks.add(new IncludedFreeExpressionTask(expression, node, strides, expressionOrbit));
					} else throw new IllegalStateException("This exception should never be reached");
				}

				boolean[] row = computed[expression.getEmbedding()];
				nodes.forEach(n -> row[n.getID()] = true);
			}
		}

		for (int index = 0; index < computed.length; ++index) {
			boolean[] row = computed[index];
			int[] ids = IntStream.range(1, row.length).filter(tid -> !row[tid]).toArray();
			if (ids.length > 0) copyTasks.add(new CopyTask(index, slice(ids)));
		}

		alphaTasks.trimToSize();
		hookedTasks.trimToSize();
		freeTasks.trimToSize();
		copyTasks.trimToSize();

		leftPattern = new ArrayList<JerboaRowPattern>(0);
		
		if(DEBUG) {
			long prepTime = DEBUG_CHRONO.end();
			System.out.println("\t PREPARATION TIME FOR RULE " + owner.getName()+" (ms): " + prepTime);
		}
	}

	private boolean isHooked(List<JerboaRuleNode> nodes) {
		for (JerboaRuleNode node : nodes) if (node.getID() == 0) return true;
		return false;
	}

	private int[][] slice(List<JerboaRuleNode> nodes) {
		return slice(nodes.stream().mapToInt(node -> node.getID()).toArray());
	}

	private int[][] slice(int[] array) {

		if (array.length == 0) return new int[][] {{}};

		ArrayList<int[]> pairs = new ArrayList<int[]>(array.length);			

		Arrays.sort(array);

		int id = array[0];
		int[] last = new int[] {id, id + 1};
		pairs.add(last);
		for (int i = 1; i < array.length; ++i) {
			id = array[i];
			if (last[1] == id) ++last[1];
			else {
				last = new int[] {id, id + 1};
				pairs.add(last);
			}
		}

		int[][] strides = new int[pairs.size()][2];
		for (int i = 0; i < pairs.size(); ++i) {
			int[] pair = pairs.get(i);
			strides[i][0] = pair[0];
			strides[i][1] = pair[1];
		}

		return strides;
	}

	private boolean isIncluded(JerboaOrbit orbit, List<JerboaRuleNode> nodes) {
		for (int alpha : orbit)
			for (JerboaRuleNode node : nodes) {
				if (node.getOrbit().contains(alpha)) continue;
				JerboaRuleNode next = node.alpha(alpha);
				if (Objects.nonNull(next) && nodes.contains(next)) continue;
				return false;
			}
		return true;
	}

	private boolean isIncluded(List<JerboaRuleNode> nodes, JerboaOrbit orbit) {
		//TODO
		/*
		for (JerboaRuleNode node : nodes) {

			JerboaOrbit nodeOrbit = node.getOrbit();

			int n = nodeOrbit.size();
			if (orbit.size() < n) return false;

			for (int alpha : nodeOrbit)
				if (alpha != JerboaOrbit.NOVALUE)
					if (orbit.contains(alpha)) --n;
					else return false;

			for (int alpha : orbit) 
				if (!nodeOrbit.contains(alpha)) {
					JerboaRuleNode next = node.alpha(alpha);
					if (Objects.nonNull(next) && nodes.contains(next)) --n;
					else return false;
				}

			if (n != 0) return false;	
		}

		return true;
		 */
		return false;
	}

	private JerboaOrbit simplify(List<JerboaRuleNode> nodes, JerboaOrbit orbit) {
		//TODO A verfier mais puisque que tous les noeuds isssue d'une orbite sont tous equivalents,
		// il suffit de retirer les alpas qui ne sont pas dans orbit

		JerboaRuleNode node = nodes.get(0);
		JerboaOrbit nodeOrbit = node.getOrbit();

		ArrayList<Integer> alphas = new ArrayList<Integer>(orbit.size());
		for (int alpha : orbit)
			if (nodeOrbit.contains(alpha)) alphas.add(alpha);
			else {
				JerboaRuleNode next = node.alpha(alpha);
				if (Objects.nonNull(next) && nodes.contains(next)) alphas.add(alpha);
			}

		return JerboaOrbit.orbit(alphas);
	}

	@Override
	public JerboaRuleResult applyRule(JerboaGMap gmap, JerboaInputHooks hooks) throws JerboaException {

		if (hooks.sizeCol() != 1)
			throw new JerboaRuleApplicationException(owner, "Wrong selection, " + getName() + " need exactly 1 node.");

		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		List<JerboaDart> leftDarts = gmap.orbit(hooks.dart(0), owner.getLeftRuleNode(0).getOrbit());

		if (Objects.nonNull(filterTask)) {
			Optional<JerboaDart> optional = filterTask.filter(leftDarts);
			if (optional.isPresent()) throw new JerboaRuleAppFoldException("Fold detected for dart " + optional.get());
		}

		leftPattern = leftDarts.stream().map(dart -> new JerboaRowPattern(dart)).collect(Collectors.toList());

		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t SEARCH LEFT PATTERN: " + time);
		}
		
		JerboaNodePrecondition precondition = owner.getLeftRuleNode(0).getNodePrecondition();
		if (Objects.nonNull(precondition) && !leftPattern.parallelStream().allMatch(row -> precondition.eval(gmap, row))) 
			throw new JerboaRulePreconditionFailsException(owner, "Node precondition has failed.");

		if (owner.hasMidprocess() && !owner.midprocess(gmap, leftPattern)) 
			throw new JerboaRulePreconditionFailsException(owner, "Rule midprocess has failed.");

		int k = leftDarts.size();
		int m = gmap.getDimension() + 1;
		int n = owner.getRight().size();

		if(DEBUG) {
			DEBUG_CHRONO.start();
		}		
		
		int[] leftMatrix = Adjacencies.byTag1D(gmap, leftDarts);
		int[] rightMatrix = new int[n * k * m];

		for (AlphaTask task : alphaTasks) task.connect(leftMatrix, rightMatrix, k, m);
		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t TOPO UPDATE LOCALLY: " + time);
		}
		
		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		ArrayList<JerboaDart> rightDarts = new ArrayList<>(leftDarts);
		Collections.addAll(rightDarts, gmap.addNodes((n - 1) * k));

		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t DUPLICATION: " + time);
		}
		
		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		for (FreeExpressionTask task : freeTasks) task.computeAndSpread(gmap, rightDarts, rightMatrix, k, m);
		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t CONSERVE CODE: " + time);
		}
		
		
		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		for (HookedExpressionTask task : hookedTasks) task.compute(gmap, rightMatrix, k, m);
		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t CALL USER CODE: " + time);
		}
		
		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		IntStream.range(0, rightDarts.size()).parallel().forEach(tid -> {
			JerboaDart dart = rightDarts.get(tid);
			int row = tid * m;
			for (int alpha = 0; alpha < m; ++alpha) {
				JerboaDart next = rightDarts.get(rightMatrix[row + alpha]);
				if (dart.getID() < next.getID()) dart.setAlpha(alpha, next);
			}
		});
		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t COPY LOCAL TO JERBOA DART: " + time);
		}
		
		
		if(DEBUG) {
			DEBUG_CHRONO.start();
		}
		for (HookedExpressionTask task : hookedTasks) task.speard(gmap, rightDarts, k);

		for (CopyTask task : copyTasks) task.copy(leftDarts, rightDarts);
		if(DEBUG) {
			long time = DEBUG_CHRONO.end();
			System.out.println("\t UPDATE EMBEDDINGS IN FINAL TOPO: " + time);
		}
		
		
		JerboaRuleResult result = new JerboaRuleResult(owner);
		for (int i = 0; i < n; ++i) result.get(i).addAll(rightDarts.subList(i * k, (i + 1) * k));

		if (owner.hasPostprocess()) owner.postprocess(gmap, result);

		return result;
	}

	@Override
	public List<JerboaRowPattern> getLeftPattern() {
		return leftPattern;
	}

	@Override
	public int countCorrectLeftRow() {
		return leftPattern.size();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("Rule: " + owner.getFullname() + "\n");

		builder.append("Filter tasks:\n");
		builder.append(filterTask + "\n");

		builder.append("Alpha tasks:\n");
		for (AlphaTask task: alphaTasks) builder.append(task + "\n");

		builder.append("Hooked expression tasks:\n");
		for (HookedExpressionTask task: hookedTasks) builder.append(task + "\n");

		builder.append("Free expression tasks:" + "\n");
		for (FreeExpressionTask task: freeTasks) builder.append(task + "\n");

		builder.append("Copy tasks:\n");
		for (CopyTask task: copyTasks) builder.append(task + "\n");

		return builder.toString();
	}

	
	@Override
	public String getName() {
		return "augmentationParallelV1";
	}
}
