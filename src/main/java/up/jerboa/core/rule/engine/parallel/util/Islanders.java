package up.jerboa.core.rule.engine.parallel.util;

import java.util.stream.IntStream;

import up.jerboa.core.util.Pair;

public abstract class Islanders {

	public static final int SKIPPED = -1;
	
	private Islanders() {}

	public abstract Pair<int[], int[]> compute();

	public static class IslandsAdjacency1D extends Islanders {

		private final int[] matrix;
		private final int m;
		private final int[][] slices;
		private final int[] alphas;

		private volatile boolean continueKernel;

		public IslandsAdjacency1D(int[] matrix, int m, int[][] slices, int[] alphas) {
			this.matrix = matrix;
			this.m = m;
			this.slices = slices;
			this.alphas = alphas;
		}

		@Override
		public Pair<int[], int[]> compute() {
			if (slices.length > 1) return sparse();
			else return dense();
		}

		private Pair<int[], int[]> dense() {

			final int begin = slices[0][0];
			final int end = slices[0][1];
			final int n = end - begin;

			final int[][] swaps = {IntStream.range(begin, end).toArray(), new int[n]};
			int first = 0;
			int second = 1;

			continueKernel = true;
			while (continueKernel) {

				continueKernel = false;

				final int[] FIRST = swaps[first];
				final int[] SECOND = swaps[second];

				IntStream.range(0, n).parallel()
				.forEach(tid -> {

					final int current = FIRST[tid];
					final int row = (begin + tid) * m;

					int next = Integer.MAX_VALUE;
					for (int i = 0; i < alphas.length; ++i) {
						final int tr = FIRST[matrix[row + alphas[i]] - begin];
						if (tr < next) next = tr;
					}

					if (next < current) {
						SECOND[tid] = next;
						if (!continueKernel) continueKernel = true;
					} else SECOND[tid] = current;
				});

				second = first;
				first = (first + 1) % 2;
			}

			final int[] TR = swaps[first];
			final int[] TF = IntStream.range(0, n).parallel()
					.filter(tid -> TR[tid] == tid + begin)
					.map(tid -> TR[tid]).toArray();

			return new Pair<>(TR, TF);
		}

		private Pair<int[], int[]> sparse() {

			final int begin = slices[0][0];
			final int end = slices[slices.length - 1][1];
			final int n = end - begin;

			final boolean[] mask = new boolean[n];
			IntStream.range(0, slices.length).parallel()
			.flatMap(tid -> IntStream.range(slices[tid][0], slices[tid][1]))
			.forEach(tid -> mask[tid] = true);

			final int[][] swaps = {IntStream.range(begin, end).toArray(), new int[n]};
			int first = 0;
			int second = 1;
			
			continueKernel = true;
			while (continueKernel) {

				continueKernel = false;

				final int[] FIRST = swaps[first];
				final int[] SECOND = swaps[second];
				
				IntStream.range(0, n).parallel()
				.filter(tid -> mask[tid])
				.forEach(tid -> {

					final int current = FIRST[tid];
					final int row = (begin + tid) * m;

					int next = Integer.MAX_VALUE;
					for (int i = 0; i < alphas.length; ++i) {
						final int tr = FIRST[matrix[row + alphas[i]] - begin];
						if (tr < next) next = tr;
					}

					if (next < current) {
						SECOND[tid] = next;
						if (!continueKernel) continueKernel = true;
					} else SECOND[tid] = current;
				});

				second = first;
				first = (first + 1) % 2;
			}
			
			final int[] TR = swaps[first];
			IntStream.range(0, n).parallel()
			.forEach(tid -> {
				if (!mask[tid]) TR[tid] = SKIPPED;
			});
			
			final int[] TF = IntStream.range(0, n).parallel()
					.filter(tid -> mask[tid])
					.filter(tid -> TR[tid] == tid + begin)
					.map(tid -> TR[tid]).toArray();

			return new Pair<>(TR, TF);
		}

	}

}
