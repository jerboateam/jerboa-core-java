package up.jerboa.core.rule.engine.parallel.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaNoFreeTagException;

public class Islands {

	public static final int SKIPPED = -1;
	
	@FunctionalInterface
	public static interface Island {

		int[] apply(JerboaGMap gmap, List<JerboaDart> darts, JerboaOrbit orbit) throws JerboaException;

	}

	private static volatile boolean continueKernel;

	private Islands() {}

	public static int[] island1D(int[] matrix, int m, int begin, int end, int[] alphas) {

		int n = end - begin;

		int[][] swaps = {IntStream.range(begin, end).toArray(), new int[n]};
		int first = 0;
		int second = 1;

		continueKernel = true;

		for (int i = 1; continueKernel && i < n; i *= 2) {

			continueKernel = false;

			for (int j = 0; j < alphas.length; ++j) {

				final int[] FIRST = swaps[first];
				final int[] SECOND = swaps[second];
				final int ALPHA = alphas[j];

				IntStream.range(0, n).parallel().forEach(id -> {

					int current = FIRST[id];
					int next = FIRST[matrix[(begin + id) * m + ALPHA] - begin];

					if (next < current) {
						SECOND[id] = next;
						if (!continueKernel) continueKernel = true;
					} else SECOND[id] = current;
				});

				second = first;
				first = (first + 1) % 2;
			}

		}

		return swaps[first];
	}

	public static int[] island1D(int[] matrix, int m, int[][] strides, int[] alphas) {

		int begin = strides[0][0];
		int end = strides[strides.length - 1][1];

		int n = end - begin;

		boolean[] mask = new boolean[n];
		for (int[] row : strides) Arrays.fill(mask, row[0] - begin, row[1] - begin, true);

		int[][] swaps = {IntStream.range(begin, end).toArray(), new int[n]};
		int first = 0;
		int second = 1;

		continueKernel = true;

		for (int i = 1; continueKernel && i < n; i *= 2) {

			continueKernel = false;

			for (int j = 0; j < alphas.length; ++j) {

				final int[] FIRST = swaps[first];
				final int[] SECOND = swaps[second];
				final int ALPHA = alphas[j];

				IntStream.range(0, n).parallel().forEach(id -> {

					if (mask[id]) {

						int current = FIRST[id];
						int next = FIRST[matrix[(begin + id) * m + ALPHA] - begin];

						if (next < current) {
							SECOND[id] = next;
							if (!continueKernel) continueKernel = true;
						} else SECOND[id] = current;
					}
				});

				second = first;
				first = (first + 1) % 2;
			}

		}
		
		int[] TF = swaps[first];
		IntStream.range(0, n).parallel().forEach(id -> {
			if (!mask[id]) TF[id] = SKIPPED;
		});
		
		return TF;
	}

	public static int[] island2D(int[][] matrix, int begin, int end, int[] alphas) {
		
		int n = end - begin;

		int[][] swaps = {IntStream.range(begin, end).toArray(), new int[n]};
		int first = 0;
		int second = 1;

		continueKernel = true;

		for (int i = 1; continueKernel && i < n; i *= 2) {

			continueKernel = false;

			for (int j = 0; j < alphas.length; ++j) {

				final int[] FIRST = swaps[first];
				final int[] SECOND = swaps[second];
				final int ALPHA = alphas[j];

				IntStream.range(0, n).parallel().forEach(id -> {

					int current = FIRST[id];
					int next = FIRST[matrix[begin + id][ALPHA] - begin];

					if (next < current) {
						SECOND[id] = next;
						if (!continueKernel) continueKernel = true;
					} else SECOND[id] = current;
				});

				second = first;
				first = (first + 1) % 2;
			}

		}
		
		return swaps[first];
	}

	public static int[] island2D(int[][] matrix, int[][] strides, int[] alphas) {

		int begin = strides[0][0];
		int end = strides[strides.length - 1][1];

		int n = end - begin;

		boolean[] mask = new boolean[n];
		for (int[] row : strides) Arrays.fill(mask, row[0] - begin, row[1] - begin, true);

		int[][] swaps = {IntStream.range(begin, end).toArray(), new int[n]};
		int first = 0;
		int second = 1;

		continueKernel = true;

		for (int i = 1; continueKernel && i < n; i *= 2) {

			continueKernel = false;

			for (int j = 0; j < alphas.length; ++j) {

				final int[] FIRST = swaps[first];
				final int[] SECOND = swaps[second];
				final int ALPHA = alphas[j];

				IntStream.range(0, n).parallel().forEach(id -> {

					if (mask[id]) {

						int current = FIRST[id];
						int next = FIRST[matrix[begin + id][ALPHA] - begin];

						if (next < current) {
							SECOND[id] = next;
							if (!continueKernel) continueKernel = true;
						} else SECOND[id] = current;
					}
				});

				second = first;
				first = (first + 1) % 2;
			}

		}
		
		int[] TF = swaps[first];
		IntStream.range(0, n).parallel().forEach(id -> {
			if (!mask[id]) TF[id] = SKIPPED;
		});
		
		return TF;
	}

	public static int[] byTag(JerboaGMap gmap, List<JerboaDart> darts, JerboaOrbit orbit) throws JerboaNoFreeTagException {

		int n = darts.size();
		int m = orbit.size();

		int[] tags = {gmap.getFreeTag(), gmap.getFreeTag()};
		int first = 0;
		int second = 1;

		IntStream.range(0, n).parallel().forEach(id -> darts.get(id).setTag(tags[0], id));

		continueKernel = true;

		for (int i = 1; continueKernel && i < n; i *= 2) {

			continueKernel = false;

			for (int j = 0; j < m; ++j) {

				final int FIRST_TAG = tags[first];
				final int SECOND_TAG = tags[second];
				final int ALPHA = orbit.get(j);

				darts.parallelStream().forEach(dart -> {

					int current = dart.getTag(FIRST_TAG);
					int next = dart.alpha(ALPHA).getTag(FIRST_TAG);

					if (next < current) {
						dart.setTag(SECOND_TAG, next);
						if (!continueKernel) continueKernel = true;
					} else dart.setTag(SECOND_TAG, current);

				});

				second = first;
				first = (first + 1) % 2;
			}
		}

		final int FIRST_TAG = tags[first];
		int[] TF = darts.stream().mapToInt(dart -> dart.getTag(FIRST_TAG)).toArray();

		gmap.freeTag(tags[0]);
		gmap.freeTag(tags[1]);

		return TF;
	}

	public static int[] byAdjacency1D(JerboaGMap gmap, List<JerboaDart> darts, JerboaOrbit orbit, 
			Adjacencies.Adjacency1D adjacency) throws JerboaException {
		int[] matrix = adjacency.apply(gmap, darts, orbit);
		int[] alphas = IntStream.range(0, orbit.size()).toArray(); 
		return island1D(matrix, orbit.size(), 0, darts.size(), alphas);
	}

	public static int[] byAdjacency2D(JerboaGMap gmap, List<JerboaDart> darts, JerboaOrbit orbit, 
			Adjacencies.Adjacency2D adjacency) throws JerboaException {
		int[][] matrix = adjacency.apply(gmap, darts, orbit);
		int[] alphas = IntStream.range(0, orbit.size()).toArray(); 
		return island2D(matrix, 0, darts.size(), alphas);
	}

}
