package up.jerboa.core.rule.engine.parallel.util;

import java.util.Arrays;
import java.util.BitSet;
import java.util.function.IntConsumer;
import java.util.function.IntPredicate;
import java.util.stream.Collector;
import java.util.stream.IntStream;

/**
 * This class contains static methods to compute unique values from int arrays.
 * 
 * @author Pierre Bourquat
 */
public class Uniques {
	
	/**
	 * Functional interface to use unique method as a lambda expression.
	 * 
	 * @author Pierre Bourquat
	 */
	@FunctionalInterface
	public static interface Unique {
		
		/**
		 * Compute distinct elements. 
		 * 
		 * @param array Input array.
		 * @return Distinct elements.
		 */
		int[] apply(int[] array);
		
	}

	/** Private constructor. */
	private Uniques() {}

	/**
	 * Uses stream API to keep distinct value.
	 * 
	 * OpenJDK explains that distinct method on IntStream is not optimized.
	 * 
	 * @param array Input values.
	 * @return Distinct values.
	 */
	public static int[] byStream(int[] array) {
		return Arrays.stream(array).distinct().toArray();
	}

	/**
	 * Computes distinct values using parallel algorithm.
	 * 
	 * Step 1 : The array is sorted.
	 * Step 2 : Each none consecutive duplicated value are set to 1 and other to 0 in an offset array.
	 * Step 3 : Perform a scan over the offset array to compute the offset for each value of the input array.
	 * Step 4 : Gather duplicated values using the offset array.
	 * 
	 * @param array Input values.
	 * @return Distinct values.
	 */
	public static int[] byScan(int[] array) {

		int n = array.length;

		int[] copy = Arrays.copyOf(array, n);
		Arrays.parallelSort(copy);

		int[][] swap = {new int[n], new int[n]};
		int first = 0;
		int second = 1;
		
		IntStream.range(1, n).parallel().forEach(id -> {
			if (copy[id - 1] != copy[id]) swap[0][id] = 1;
		});

		for (int offset = 1; offset < n; offset *= 2) {
			
			final int FIRST = first;
			final int SECOND = second;
			final int OFFSET = offset;
			
			IntStream.range(0, n).parallel().forEach(id -> {
				swap[SECOND][id] = OFFSET <= id ? swap[FIRST][id] + swap[FIRST][id - OFFSET] : swap[FIRST][id];
			});
			
			second = first;
			first = (first + 1) % 2;
		}
		
		int[] result = new int[swap[first][n - 1] + 1];
		
		final int FIRST = first;
		IntStream.range(0, n).parallel().forEach(id -> {
			int dest = swap[FIRST][id];
			result[dest] = copy[id];		
		});
		
		return result;
	}
	
	/**
	 * Suitable predicate for IntStream distinct operation.
	 * This predicate uses a BitSet.
	 * 
	 * The predicate knows the min and max of the IntStrean on which it is applied to reduce the memory consumption.
	 * Values lesser than min always fail the predicate.
	 * 
	 * @author Pierre Bourquat
	 */
	private static class UniquePredicate implements IntPredicate {
		
		/** Min value. */
		private final int min;
		/** Max value. */
		private final int max;
		/** Bits array to store seen values. */
		private final BitSet bits;
		
		/**
		 * Constructor.
		 * 
		 * Knowing min and max value of the IntStream on which the predicate is applied 
		 * allows to reduce the size of the BitSet and prevent its resizing.
		 * 
		 * @param min Inclusive min value.
		 * @param max Exclusive max value.
		 */
		public UniquePredicate(int min, int max) {
			this.min = min;
			this.max = max;
			bits = new BitSet(max - min);
		}
		
		@Override
		public boolean test(int value) {
			
			if (value < min) return false;
			if (max <= value) return false;
			
			value -= min;
			if (!bits.get(value)) {
				bits.set(value);
				return true;
			}
			
			return false;
		}		
		
	}
	
	/**
	 * Computes distinct values by using a BitSet as predicate.
	 * 
	 * Values not include in [min, max[ are not kept.
	 * 
	 * @param array Array of values.
	 * @param min Inclusive min value of the array.
	 * @param max Exclusive max value in the array
	 * @return Distinct values of array.
	 */
	public static int[] byBitSet(int[] array, int min, int max) {		
		return Arrays.stream(array).filter(new UniquePredicate(min, max)).toArray();
	}
	
	/**
	 * MinMax class used to collect min and max value of a boxed IntStream.
	 * 
	 * @author pbourqua
	 *
	 */
	private static class MinMax implements IntConsumer {

		/** Min value. */
		private int min = Integer.MAX_VALUE;
		/** Max value. */
		private int max = Integer.MIN_VALUE;
		
		/** Default constructor. */
		public MinMax() {}
		
		@Override
		public void accept(int value) {
			if (value < min) min = value;
			if (max < value) max = value;
		}
		
		/**
		 * Combines a MinMax with this. 
		 * This will contain min and max values of this and that.
		 * 
		 * @param that Combined MinMax.
		 */
		public void combine(MinMax that) {
			if (that.min < min) min = that.min;
			if (max < that.max) max = that.max;
		}
		
		public UniquePredicate finish() {
			return new UniquePredicate(min, max);
		}
		
		/**
		 * Returns a MinMax Collector for boxed IntStream.
		 * @return MinMax collector.
		 */		
		public static Collector<Integer, MinMax, UniquePredicate> intMinMax() {
			return Collector.of(
					MinMax::new, 
					MinMax::accept, 
					(l, r) -> { l.combine(r); return l; },
					MinMax::finish, Collector.Characteristics.UNORDERED);
		}
		
	}
	
	/**
	 * Computes distinct values by using a BitSet as predicate with two passes.
	 * 
	 * The first pass compute min and max value of the array to reduce the memory consumption of the BitSet.
	 * The second pass return the distinct values.
	 * 
	 * @param array Input array.
	 * @return Distinct values of array.
	 */
	public static int[] byBitSet(int[] array) {
		return Arrays.stream(array).filter(Arrays.stream(array).boxed().collect(MinMax.intMinMax())).toArray();
	}

}
 