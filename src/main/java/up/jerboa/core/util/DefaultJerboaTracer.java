/**
 * 
 */
package up.jerboa.core.util;

/**
 * @author Hakim Belhaouari
 *
 */
public class DefaultJerboaTracer extends JerboaTracer {

	private transient String title;
	private transient int min;
	private transient int max;
	
	/**
	 * 
	 */
	public DefaultJerboaTracer() {
		title= null;
		min = 0;
		max = 100;
	}

	/**
	 * @see up.jerboa.core.util.JerboaTracer#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @see up.jerboa.core.util.JerboaTracer#setMinMax(int, int)
	 */
	@Override
	public void setMinMax(int min, int max) {
		this.min = min;
		this.max = max;
	}

	/**
	 * @see up.jerboa.core.util.JerboaTracer#report(java.lang.String, int)
	 */
	@Override
	public void report(String message, int step) {
		StringBuilder sb = new StringBuilder();
		if(title != null && !title.isEmpty()) {
			sb.append("[").append(title).append("] ");
		}
		if(step < 0)
			sb.append("[").append(min).append("|").append(step).append("|").append(max).append("] ");
		sb.append(message);
		System.out.println(sb.toString());
	}
	
	/**
	 * @see up.jerboa.core.util.JerboaTracer#report(java.lang.String, int)
	 */
	@Override
	public void report(String message) {
		report(message,-1);
	}

	@Override
	public void done() {
		System.out.println("[DONE] "+title);
		title = null;
	}
}
