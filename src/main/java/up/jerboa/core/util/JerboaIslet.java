package up.jerboa.core.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaMark;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;
import up.jerboa.exception.JerboaNoFreeTagException;

/**
 * 
 * @author Pierre Bourquat
 *
 */
public class JerboaIslet {


	public static int[] islet(JerboaGMap gmap, List<JerboaDart> start, JerboaOrbit orbit) throws JerboaNoFreeTagException {
		final int refTag = gmap.getFreeTag();
		try {
						
			int[] tab = orbit.tab();
			boolean again = true;
			final int size = start.size();
			int[] a = new int[size];
			int[] b = new int[size];

			// Initialisation
			for (JerboaDart dart : gmap) {
				dart.setTag(refTag, -1);
			}
			final int[] fa = a;
			final int[] fb = b;
			IntStream.range(0, size).parallel().forEach(i -> { 
				JerboaDart d = start.get(i);
				fa[i] = d.getID();
				fb[i] = d.getID();
				d.setTag(refTag, i);
			});


			while(again) {
				again = false;
				for(int dim : tab) {
					final int[] input = a;
					final int[] output = b;
					boolean modif = IntStream.range(0, size).parallel().map(i -> {
						JerboaDart d = start.get(i);
						JerboaDart v = d.alpha(dim);
						int vid = v.getTag(refTag);
						int aid = input[i];
						if(vid != -1) {
							int vaid = input[vid];
							if (vaid < aid) {
								output[i] = vaid;
								return 1;
							}
							else {
								output[i] = aid;
							}
						}
						return 0;
						// else on sort de l'ilot donc inutile.
					}).sum() > 0;

					again = modif || again;
					int[] tmp = a;
					a = b;
					b = tmp;
				}
			}

			return a;
		}
		finally {
			if(refTag != -1)
				gmap.freeTag(refTag);
		}
	}
	
	
	public static int[] islet_seq(JerboaGMap gmap, List<JerboaDart> start, JerboaOrbit orbit) throws JerboaNoFreeTagException {
		final int refTag = gmap.getFreeTag();
		try {
						
			int[] tab = orbit.tab();
			boolean again = true;
			final int size = start.size();
			int[] a = new int[size];
			int[] b = new int[size];

			// Initialisation
			for (JerboaDart dart : gmap) {
				dart.setTag(refTag, -1);
			}
			final int[] fa = a;
			final int[] fb = b;
			IntStream.range(0, size).sequential().forEach(i -> { 
				JerboaDart d = start.get(i);
				fa[i] = d.getID();
				fb[i] = d.getID();
				d.setTag(refTag, i);
			});


			while(again) {
				again = false;
				for(int dim : tab) {
					final int[] input = a;
					final int[] output = b;
					boolean modif = IntStream.range(0, size).sequential().map(i -> {
						JerboaDart d = start.get(i);
						JerboaDart v = d.alpha(dim);
						int vid = v.getTag(refTag);
						int aid = input[i];
						if(vid != -1) {
							int vaid = input[vid];
							if (vaid < aid) {
								output[i] = vaid;
								return 1;
							}
							else {
								output[i] = aid;
							}
						}
						return 0;
						// else on sort de l'ilot donc inutile.
					}).sum() > 0;

					again = modif || again;
					int[] tmp = a;
					a = b;
					b = tmp;
				}
			}

			return a;
		}
		finally {
			if(refTag != -1)
				gmap.freeTag(refTag);
		}
	}
	
	
	public static List<JerboaDart> islet_seq_alt(JerboaGMap gmap, List<JerboaDart> start, JerboaOrbit orbit) throws JerboaNoFreeTagException {
		final JerboaMark mark = gmap.creatFreeMarker();
		List<JerboaDart> list = new ArrayList<JerboaDart>();
		for(JerboaDart s : gmap) {
			if(s.isNotMarked(mark)) {
				list.add(s);
				try {
					gmap.markOrbit(s, orbit, mark);
				} catch (JerboaException e) {
					e.printStackTrace();
				}
			}
		}
		return list;
	}
	
}
