package up.jerboa.core.util;

import java.util.List;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaOrbit;

public interface JerboaOrbitter {
	List<JerboaDart> orbit(JerboaOrbit orbit, JerboaDart start);
}
