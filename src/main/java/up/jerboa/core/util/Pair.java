/**
 * 
 */
package up.jerboa.core.util;

/**
 * @author Hakim Belhaouari
 *
 */
public class Pair<L,R> {

	private L l;
	private R r;
	/**
	 * 
	 */
	public Pair(L l , R r) {
		this.l = l;
		this.r = r;
	}
	
	public L l() {
		return l;
	}
	
	public R r() {
		return r;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Pair) {
			Pair<?,?> p = (Pair<?, ?>) obj;
			return l.equals(p.l) && r.equals(p.r);
		}
		return super.equals(obj);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<").append(l).append(" ; ").append(r).append(">");
		return sb.toString();
	}
}
