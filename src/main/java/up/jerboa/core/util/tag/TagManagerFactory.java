package up.jerboa.core.util.tag;

/**
 * Tag manager factory.
 * 
 * @author Pierre Bourquat
 */
public class TagManagerFactory {
	
	/**
	 * Returns the best tag manager according to the specified capacity.
	 * 
	 * @param capacity Tag manager capacity.
	 * @return Tag manager according to the capacity.
	 */
	public TagManager create(int capacity) {
		if (capacity <= LongTagManager.MAXIMAL_CAPACITY) return new LongTagManager(capacity);
		return new LimitlessTagManager(capacity);
	}

}
