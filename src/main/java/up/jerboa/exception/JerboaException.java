/**
 * 
 */
package up.jerboa.exception;

/**
 * @author Hakim Belhaouari
 *
 */
public class JerboaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7166823441834931287L;

	/**
	 * 
	 */
	public JerboaException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public JerboaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public JerboaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public JerboaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
