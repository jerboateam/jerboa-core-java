/**
 * 
 */
package up.jerboa.exception;


/**
 * @author hakim
 *
 */
public class JerboaGMapDuplicateException extends JerboaException {

	private static final long serialVersionUID = 5698336383672233548L;

	/**
	 * 
	 */
	public JerboaGMapDuplicateException() {
		
	}

	/**
	 * @param message
	 */
	public JerboaGMapDuplicateException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public JerboaGMapDuplicateException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public JerboaGMapDuplicateException(String message, Throwable cause) {
		super(message, cause);
		
	}

}
