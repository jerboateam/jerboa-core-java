package up.jerboa.exception;

import up.jerboa.core.JerboaModeler;

public class JerboaInconsistentApplication extends JerboaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4174120708521728188L;

	public JerboaInconsistentApplication() {
		// TODO Auto-generated constructor stub
	}

	public JerboaInconsistentApplication(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public JerboaInconsistentApplication(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public JerboaInconsistentApplication(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public JerboaInconsistentApplication(JerboaModeler owner, JerboaModeler modeler) {
		super("Application a rule for different modeler!");
	}

}
