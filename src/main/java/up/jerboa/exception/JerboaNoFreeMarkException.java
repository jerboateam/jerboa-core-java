/**
 * 
 */
package up.jerboa.exception;

/**
 * @author Hakim Belhaouari
 *
 */
public class JerboaNoFreeMarkException extends JerboaException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6824519655066337723L;

	/**
	 * 
	 */
	public JerboaNoFreeMarkException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public JerboaNoFreeMarkException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public JerboaNoFreeMarkException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public JerboaNoFreeMarkException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
