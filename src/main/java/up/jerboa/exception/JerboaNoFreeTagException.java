package up.jerboa.exception;

/**
 * This exception should be raised when no more tags index are available.
 * 
 * @author Pierre Bourquat
 */
public class JerboaNoFreeTagException extends JerboaException {

	/** Auto generated serial uid. */
	private static final long serialVersionUID = 8630959602199802498L;

	/**
	 * Call super constructor.
	 */
	public JerboaNoFreeTagException() {
		super();
	}

	/**
	 * Call super constructor.
	 * 
	 * @param message Exception message.
	 * @param cause Exception cause.
	 */
	public JerboaNoFreeTagException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Call super constructor.
	 * 
	 * @param message Exception message.
	 */
	public JerboaNoFreeTagException(String message) {
		super(message);
	}

	/**
	 * Call super constructor.
	 * 
	 * @param cause Exception cause.
	 */
	public JerboaNoFreeTagException(Throwable cause) {
		super(cause);
	}
	
}
