package up.jerboa.exception;

import up.jerboa.core.JerboaRuleAtomic;
import up.jerboa.core.rule.JerboaRowPattern;

public class JerboaRuleAppIncompatibleOrbit extends JerboaRuleApplicationException {

	private int r;
	private JerboaRowPattern matrix;
	private String message;
	private JerboaRuleAtomic rule;

	public JerboaRuleAppIncompatibleOrbit(JerboaRuleAtomic rule,int r, JerboaRowPattern matrix) {
		this.r = r;
		this.rule = rule;
		this.matrix = matrix;
	}

	private static final long serialVersionUID = 209896361402674122L;

	
	@Override
	public String getMessage() {
		if (message == null) {
			StringBuilder sb = new StringBuilder("[");
			sb.append(rule.getName()).append("] ").append("LINE");
			sb.append(r).append(" -> ").append(matrix.toString());
			message = sb.toString();
		}
		return message;
	}
}
