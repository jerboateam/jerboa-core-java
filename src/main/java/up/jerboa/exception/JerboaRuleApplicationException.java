/**
 * 
 */
package up.jerboa.exception;

import up.jerboa.core.JerboaRuleAtomic;

/**
 * @author hakim
 *
 */
public class JerboaRuleApplicationException extends JerboaException {
	private static final long serialVersionUID = 1996693931927684631L;
	private JerboaRuleAtomic rule;

	/**
	 * 
	 */
	public JerboaRuleApplicationException() {
	}

	/**
	 * @param message
	 */
	public JerboaRuleApplicationException(JerboaRuleAtomic rule, String message) {
		super(rule.getName()+": "+message);
		this.rule = rule;
	}
	
	public JerboaRuleApplicationException(String message) {
		super(message);
		this.rule = null;
	}
	
	public JerboaRuleAtomic getRule() {
		return rule;
	}
}
