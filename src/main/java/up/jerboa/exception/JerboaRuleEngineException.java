package up.jerboa.exception;

import up.jerboa.core.JerboaRuleAtomic;

public class JerboaRuleEngineException extends JerboaRuntimeException {

	private static final long serialVersionUID = 2129237471657722482L;

	public JerboaRuleEngineException() {
	}

	public JerboaRuleEngineException(String message) {
		super(message);
	}

	public JerboaRuleEngineException(Throwable cause) {
		super(cause);
	}

	public JerboaRuleEngineException(String message, Throwable cause) {
		super(message, cause);
	}

	public JerboaRuleEngineException(JerboaRuleAtomic owner, String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
