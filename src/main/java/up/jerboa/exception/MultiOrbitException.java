package up.jerboa.exception;

import java.util.ArrayList;
import java.util.Collection;

import up.jerboa.core.JerboaDart;
import up.jerboa.util.Triplet;

public class MultiOrbitException extends JerboaException {

	private static final long serialVersionUID = 8156691755026725231L;
	
	private ArrayList<Triplet<Integer, JerboaDart, JerboaDart>> errors;

	public MultiOrbitException() {
		errors = new ArrayList<>();
	}

	public MultiOrbitException(String message) {
		super(message);
		errors = new ArrayList<>();
	}

	public MultiOrbitException(Throwable cause) {
		super(cause);
		errors = new ArrayList<>();
	}

	public MultiOrbitException(String message, Throwable cause) {
		super(message, cause);
		errors = new ArrayList<>();
	}
	
	
	public void add(Integer dart, JerboaDart d1, JerboaDart d2) {
		errors.add(new Triplet<Integer, JerboaDart, JerboaDart>(dart, d1, d2));
	}
	
	
	Collection<Triplet<Integer, JerboaDart, JerboaDart>> getErrors() {
		return errors;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(getLocalizedMessage());
		sb.append(errors.toString());
		return sb.toString();
	}

}
