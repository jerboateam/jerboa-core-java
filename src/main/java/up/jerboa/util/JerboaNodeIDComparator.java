package up.jerboa.util;

import java.util.Comparator;

import up.jerboa.core.JerboaDart;

public class JerboaNodeIDComparator implements Comparator<JerboaDart> {

	@Override
	public int compare(JerboaDart o1, JerboaDart o2) {
		return Integer.compare(o1.getID(), o2.getID());
	}
}
