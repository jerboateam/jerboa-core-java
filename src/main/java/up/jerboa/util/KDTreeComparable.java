package up.jerboa.util;

import java.util.Comparator;

public interface KDTreeComparable<T> extends Comparator<T> {
	 public void setDimension(int i);
	 public int getDimension();
	 
	 public float extractValue(T element);
}
