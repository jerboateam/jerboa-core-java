package up.jerboa.util;

import up.jerboa.core.JerboaDart;

public class OrbitBuffer implements Comparable<OrbitBuffer> {
	private JerboaDart dart;
	private KeyOrbitInterface koi;
	
	
	public OrbitBuffer(JerboaDart dart, KeyOrbitInterface koi) {
		this.dart = dart;
		this.koi = koi;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof OrbitBuffer) {
			OrbitBuffer ob = (OrbitBuffer) obj;
			if(dart.equals(ob.dart) && koi.equals(ob.koi)) {
				return true;
			}
		}
		
		return super.equals(obj);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("<|").append(dart).append("|").append(koi).append("|>");
		return sb.toString();
	}

	@Override
	public int compareTo(OrbitBuffer o) {
		int i = dart.compareTo(o.dart);
		if(i == 0) {
			return koi.compareTo(o.koi);
		}
		return i;
	}
	
}
