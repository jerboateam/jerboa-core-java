package up.jerboa.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TreeMap;


/**
 * This structure represents an HashMap where a key may have multiple value. This implementation encompasses a simple HashMap where the value is a
 * list of values. Consequently you must be aware of the returned value must not be modified by user! This class is not complete but may help in
 * many implementations.
 * @author Hakim
 *
 * @param <K> type of key
 * @param <V> type of values
 */
public class TreeMapList<K, V> implements Iterable<Entry<K, List<V>>> {

	private TreeMap<K, List<V>> data;
	
	/**
	 * Construct an empty structure.
	 */
	public TreeMapList() {
		data = new TreeMap<>();
	}
	
	/**
	 * Test if the current structure is empty or not. 
	 * @return True is the structure does not contain any element inside, False otherwise.
	 */
	public boolean isEmpty() {
		return data.isEmpty();
	}
	
	/**
	 * Method for inserting new pair in the structure. If the key does not exist, it adds automatically a new entry in the internal structure.
	 * @param cle: key to add
	 * @param values: first values that must be added. (possibility none)
	 */
	public void put(K cle, V v) {
		if(!data.containsKey(cle)) {
			data.put(cle, new ArrayList<>());
		}
		data.get(cle).add(v);
	}
	
	/**
	 * Method for inserting new pair in the structure. If the key does not exist, it adds automatically a new entry in the internal structure.
	 * @param cle: key to add
	 * @param values: first values that must be added. (possibility none)
	 */
	public void put(K cle, @SuppressWarnings("unchecked") V... values) {
		if(!data.containsKey(cle)) {
			data.put(cle, new ArrayList<>());
		}
		for (V v : values) {
			data.get(cle).add(v);	
		}
	}
	
	public void putAll(K cle, Collection<? extends V> values) {
		if(!data.containsKey(cle)) {
			data.put(cle, new ArrayList<>());
		}
		data.get(cle).addAll(values);
	}
	
	public List<V> get(K cle) {
		if(data.containsKey(cle))
			return data.get(cle);
		else
			return new ArrayList<>();
	}
	
	public List<V> remove(K cle) {
		return data.remove(cle);
	}
	
	public void remove(K cle, V value) {
		if(data.containsKey(cle)) {
			data.get(cle).remove(value);
		}
	}
	
	public V getLast(K cle) {
		List<V> list =get(cle);
		if(list.isEmpty())
			throw new NoSuchElementException();
		else
			return list.get(list.size()-1);
	}
	
	public V getFirst(K cle) {
		List<V> list =get(cle);
		if(list.isEmpty())
			throw new NoSuchElementException();
		else
			return list.get(0);
	}
	
	public int size() {
		return data.size();
	}
	
	
	public boolean containsKey(K cle) {
		return data.containsKey(cle);
	}
	
	public K containsValue(V value) {
		for (Entry<K, List<V>> entry : data.entrySet()) {
			if(entry.getValue().contains(value))
				return entry.getKey();
		}
		return null;
	}

	@Override
	public Iterator<Entry<K, List<V>>> iterator() {
		return data.entrySet().iterator();
	}
	
	public Set<K> keys() {
		return data.keySet();
	}
	
	public Collection<List<V>> values() {
		return data.values();
	}
	
	public void clear() {
		data.clear();
	}
	
}
