/**
 * 
 */
package up.jerboa.util.avl;


/**
 * Cette interface est utilise dans mon arbre AVL.
 * Attention son fonctionnement est vraiment dependant de ma fonction.
 * 
 * @author hakim
 *
 */
public interface AVLComparator<T> {
	/**
	 * Cette fonction indique s'il faut modifier les coordonnees du points en cours de test (le premier)
	 * avec les coordonnees du second si le boolean passe en parametre une variable a etat interne.
	 * @param i
	 */
	public void setInserting(boolean i);
	
	
	/**
	 * 
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	public int compare(T arg0, T arg1);
}
