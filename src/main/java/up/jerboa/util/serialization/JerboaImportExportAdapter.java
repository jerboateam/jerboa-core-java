/**
 * 
 */
package up.jerboa.util.serialization;

import java.util.ArrayList;
import java.util.List;

import up.jerboa.core.JerboaModeler;
import up.jerboa.util.DefaultJerboaSerializerMonitor;
import up.jerboa.util.JerboaSerializerMonitor;

/**
 * @author Hakim Belhaouari
 *
 */
public abstract class JerboaImportExportAdapter<T extends EmbeddingSerialization> implements JerboaImportExportInterface {

	protected JerboaSerializerMonitor monitor;
	protected T factory;
	protected JerboaModeler modeler;
	protected ArrayList<String> formats;
	
	/**
	 * 
	 */
	public JerboaImportExportAdapter(JerboaModeler modeler, JerboaSerializerMonitor monitor, T serializer, String... extensions) {
		this.monitor = monitor;
		this.modeler = modeler;
		this.factory = serializer;
		
		if(this.monitor == null)
			this.monitor = new DefaultJerboaSerializerMonitor();
		
		if(serializer.kind() != EmbeddingSerializationKind.SAVEANDLOAD)
			throw new RuntimeException("Incompatible Serialization save/load!");
		
		formats = new ArrayList<>();
		for (String ext : extensions) {
			formats.add(ext);
		}
		
		JerboaLoadingSupportedFiles.registerImport(extensions,this.getClass());
		JerboaSavingSupportedFiles.registerExport(extensions,this.getClass());
	}

	/**
	 * @see up.jerboa.util.serialization.JerboaImportInterface#getMonitor()
	 */
	@Override
	public JerboaSerializerMonitor getMonitor() {
		return monitor;
	}

	/**
	 * @see up.jerboa.util.serialization.JerboaImportInterface#setMonitor(up.jerboa.util.JerboaSerializerMonitor)
	 */
	@Override
	public void setMonitor(JerboaSerializerMonitor monitor) {
		this.monitor = monitor;
	}
	
	@Override
	public List<String> getLoadFormats() {
		return formats;
	}
	
	@Override
	public List<String> getSaveFormats() {
		return formats;
	}
}
