package up.jerboa.util.serialization;

import java.io.File;

import javax.swing.filechooser.FileFilter;


public class JerboaSavingSupportedFiles extends FileFilter {

	public static final String[] exts = new String[] { ".moka" , ".mok", ".dot" ,".jba", ".jbz" };
	
	public JerboaSavingSupportedFiles() {
	}

	@Override
	public boolean accept(File f) {
		if(f.isDirectory())
			return true;
		else {
			boolean b = false;
			for (String ext : exts) {
				String lowcaseext = ext.toLowerCase();
				b = f.getAbsolutePath().endsWith(lowcaseext) || b;
			}
			return b;
		}
	}

	@Override
	public String getDescription() {
		return "Supported format by Jerboa";
	}

	public static void registerExport(String[] extensions,
			Class<? extends JerboaExportInterface> class1) {
		System.out.println("REGISTER EXPORTATION: "+extensions);
	}

}
