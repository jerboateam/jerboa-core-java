package up.jerboa.util.serialization.gmsh;

public class GmshPoint {
	private double x,y,z;
	private int id;
	
	public GmshPoint(int id, double x, double y, double z) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getID() {
		return id;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "<"+x+";"+y+";"+z+">";
	}
}
