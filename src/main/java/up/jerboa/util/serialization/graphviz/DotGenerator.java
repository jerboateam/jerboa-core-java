package up.jerboa.util.serialization.graphviz;

import java.awt.Color;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.JerboaDart;
import up.jerboa.util.JerboaSerializerMonitor;
import up.jerboa.util.serialization.JerboaExportAdapter;

public class DotGenerator extends JerboaExportAdapter<DotGeneratorSerializer> {

	private JerboaGMap gmap;

	private String styleAX[] = new String[] {   
			"style=solid, color=black",
			"style=dashed, color=red",
			"style=solid, color=\"blue:white:blue\"",
			"style=dashed, color=green"
	};

	private boolean keepPos = true;
	private boolean outsideLabel = false;
	private boolean nolabel = false;
	private boolean fillColor = false;
	
	private float lenoutside = 0.5f;
	private int dim;
	private float zoom = 1.f;
	private DotGenPointPlan plan = DotGenPointPlan.XZ;
	
	private boolean explode = true;
	private DotGenNodeStyle style = DotGenNodeStyle.CIRCLE;
	private float nodeSize = 0.15f;
	
	private float[] alphalen = new float[] { 1.f, 0.25f, 0.35f, 0.5f };
	private boolean linkLabelOut = false;

	private boolean freepos = false;

	
	private boolean fromcamera = false;
	private float[] matrix;

	private boolean showhook = true;

	private boolean hookforced = false;
	private boolean dispLoop = true;

	public DotGenerator(JerboaModeler modeler, JerboaSerializerMonitor monitor, DotGeneratorSerializer provider) {
		super(modeler,monitor,provider);
		this.dim = modeler.getDimension();
		
		matrix = new float[9];
		matrix[0] = 1;
		matrix[4] = 1;
		matrix[8] = 1;
		
		// information dotty accept dim [2;10] 
		// mettre un garde fou pour limiter entre 2 et 3
		
	}

	// width 	N	double	0.75	0.01
	// height 	N	double	0.5	0.02	
	
	public void save(OutputStream os) {
		this.gmap = modeler.getGMap();
		
		monitor.setMessage("Converting gmap into .dot format...");
		int length = gmap.size();
		monitor.setMinMax(0, length*2 + 1);
		int progress = 0;
		PrintStream ps = new PrintStream(os);

		ps.println("graph gmap {");
		
		ps.println("graph [dim="+dim+", dimen="+dim+", overlap=scale];");
		ps.print("node [ shape=\"");
		ps.print(style.toString().toLowerCase());
		ps.print("\", style=\"filled\", fillcolor=\"lightgray\"");
		//if(Math.abs(nodeSize - 1.0f) <= 0.01)
		{
			float w = 0.75f * nodeSize;
			float h = 0.5f * nodeSize;
			ps.print(", fixedsize=\"false\", width=\""+w+"\", height=\""+h+"\"");
		}
		
		if(outsideLabel) {
			ps.print(", label=\"\"");
		}

		ps.println(" ];");
		ps.println();
		
		ps.println("/* dart informations */");
		for (JerboaDart node : gmap) {
			int nid = node.getID();
			ps.print(""+nid+" [ ");
			if(showhook && factory != null && factory.isHook(node))
				ps.print("shape=\"double"+style.toString().toLowerCase()+"\", ");
			if(nolabel)
				ps.print("label=\"\" ,");
			else if(outsideLabel){
				ps.print("xlabel=\""+nid+"\" ,");
			}
			if(factory != null) {
				StringBuilder sb = new StringBuilder(" ");
				
				
				// if(!freepos) 
				{					
					sb = new StringBuilder(" pos=\"");
					DotGeneratorPoint point =  factory.pointLocation(node,explode);

					if(fromcamera ) {
						float x = point.x*matrix[0] + point.y*matrix[1] + point.z*matrix[2];
						float y = point.x*matrix[3] + point.y*matrix[4] + point.z*matrix[5];
						float z = point.x*matrix[6] + point.y*matrix[7] + point.z*matrix[8];
						point.x = x;
						point.y = y;
						point.z = z;
					}

					point.scale(zoom);

					switch(plan) {
					case XY: sb.append(point.x+","+point.y); break;
					case XZ: sb.append(point.x).append(',').append(point.z); break;
					case YZ: sb.append(point.y).append(',').append(point.z); break;
					case XYZ: sb.append(point.x).append(',').append(point.y).append(',').append(point.z); break;
					}

					if(keepPos)
						sb.append('!');
					else if(hookforced && factory.isHook(node)) {
						sb.append('!');
					}
					
					sb.append('"');

				}

				if(fillColor) {
					if(!freepos)
						sb.append(", ");
					sb.append("fillcolor=\""+hexcolor(factory.color(node))+"\"");
				}
				
				ps.print(sb.toString());
			}
			ps.println("];");
			
			
			progress++;
			monitor.setProgressBar(progress);
		}
		
		ps.println();
		ps.println("/* alpha relation */");
		ArrayList<StringBuilder> alphas = new ArrayList<StringBuilder>();
		for(int i = 0; i <= dim; i++) {
			alphas.add(new StringBuilder());
		}
		for (JerboaDart node : gmap) {
			int nid = node.getID();
			for(int i = 0;i <= dim;i++) {
				int vid = node.alpha(i).getID();
				if(nid < vid) {
					alphas.get(i).append(""+nid+" -- "+""+vid+" ["+styleAX[i]);
					alphas.get(i).append(", len=\""+alphalen[i]+"\", weight=\""+(i+1)+"\" ");
					alphas.get(i).append("];");
					alphas.get(i).append('\n');
				}
				if(nid == vid) {
					alphas.get(i).append(""+nid+" -- "+""+vid+" ["+styleAX[i]);
					alphas.get(i).append(", weight=\""+(i+1)+"\" ");
					alphas.get(i).append("];");
					alphas.get(i).append('\n');
				}
			}
			progress++;
			monitor.setProgressBar(progress);
		}
		for(int i = 0; i <= dim; i++) {
			ps.println(" /* alpha "+i+" */");
			ps.println(alphas.get(i).toString());
		}	
		ps.println();		
		ps.println("}");
		progress++;
		monitor.setProgressBar(progress);
	}

	String hexcolor(Color color) {
		StringBuilder sb = new StringBuilder("#");
		sb.append(Integer.toString(color.getRed(), 16));
		sb.append(Integer.toString(color.getGreen(), 16));
		sb.append(Integer.toString(color.getBlue(), 16));
		sb.append(Integer.toString(color.getAlpha(), 16));
		return sb.toString();
	}
	
	public boolean isKeepPos() {
		return keepPos;
	}


	public void setKeepPos(boolean keepPos) {
		this.keepPos = keepPos;
	}


	public boolean isNolabel() {
		return nolabel;
	}


	public void setNolabel(boolean nolabel) {
		this.nolabel = nolabel;
	}


	public boolean isFillColor() {
		return fillColor;
	}


	public void setFillColor(boolean fillColor) {
		this.fillColor = fillColor;
	}


	public float getZoom() {
		return zoom;
	}


	public void setZoom(float zoom) {
		this.zoom = zoom;
	}

	public DotGenPointPlan getPlan() {
		return plan;
	}


	public void setPlan(DotGenPointPlan plan) {
		this.plan = plan;
	}


	public boolean isExplode() {
		return explode;
	}


	public void setExplode(boolean explode) {
		this.explode = explode;
	}
	
	public DotGenNodeStyle getStyle() {
		return style;
	}


	public void setStyle(DotGenNodeStyle style) {
		this.style = style;
	}


	public float getNodeSize() {
		return nodeSize;
	}


	public void setNodeSize(float nodeSize) {
		this.nodeSize = nodeSize;
	}
	
	public float alphaLen(int dim) {
		return alphalen[dim];
	}
	public void setAlphaLen(int dim, float val) {
		alphalen[dim] = val;
	}

	public boolean isOutsideLabel() {
		return outsideLabel;
	}

	public void setOutsideLabel(boolean outsideLabel) {
		this.outsideLabel = outsideLabel;
	}

	public float getLenOutside() {
		return lenoutside;
	}

	public void setLenOutside(double lenoutside) {
		this.lenoutside = (float) lenoutside;
	}

	public boolean isLinkLabelOut() {
		return linkLabelOut;
	}

	public void setLinkLabelOut(boolean linkLabelOut) {
		this.linkLabelOut = linkLabelOut;
	}
	
	public void setForceFreePos(boolean freepos) {
		this.freepos = freepos;
	}
	public boolean isForceFreePos() {
		return this.freepos;
	}

	
	public boolean isFromCamera() {
		return this.fromcamera;
	}
	
	public void setFromCamera(boolean b) {
		this.fromcamera = b;
	}
	
	public void setMatrix(float[] mat) {
		if(mat == null) {
			matrix = new float[9];
			matrix[0] = 1;
			matrix[4] = 1;
			matrix[8] = 1;
			System.out.println("MATRIX: NULL");
		}
		else {
			this.matrix = mat;
			System.out.print("MATRIX:");
			for (float f : mat) {
				System.out.print(" "+f);
			}
			System.out.println();
		}
	}

	public void setShowHook(boolean selected) {
		this.showhook = selected;
	}

	public boolean isShowHook() {
		return showhook;
	}

	public boolean isForceHook() {
		return hookforced;
	}

	public void setForceHook(boolean selected) {
		hookforced = selected;
	}
	
}
