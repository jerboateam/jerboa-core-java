package up.jerboa.util.serialization.jba;

import java.util.ArrayList;
import java.util.List;

import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.util.Pair;
import up.jerboa.util.serialization.EmbeddingSerialization;

public interface JBBEmbeddingSerialization extends EmbeddingSerialization {

	byte[] serialize(JerboaEmbeddingInfo info, Object value);
	Object unserialize(JerboaEmbeddingInfo info, byte[] ebdline);
	
	default List<Pair<String,String>> properties() {
		return new ArrayList<>();
	}
	
	default void setproperty(String name, String value) {
		
	}
}
