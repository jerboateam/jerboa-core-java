/**
 * 
 */
package up.jerboa.util.serialization.jba;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaMark;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JBAIncompatibleDimensionException;
import up.jerboa.exception.JerboaException;
import up.jerboa.util.JerboaSerializerMonitor;
import up.jerboa.util.serialization.JerboaImportExportAdapter;
import up.jerboa.util.serialization.JerboaSerializeException;

/**
 * @author hbelhaou
 * 
 */
public class JBBFormat extends JerboaImportExportAdapter<JBBEmbeddingSerialization> {
	
	private transient int dimension;
	private transient int ebdlength;
	
	
	
	public JBBFormat(JerboaModeler modeler, JerboaSerializerMonitor monitor, JBBEmbeddingSerialization factory) {
		super(modeler,monitor,factory,".jbb");
	}
	
	protected JBBFormat(JerboaModeler modeler, JerboaSerializerMonitor monitor, JBBEmbeddingSerialization factory, String... extensions) {
		super(modeler,monitor,factory,extensions);
	}
	
	private void writeString(DataOutputStream dos, String s) throws IOException {
		dos.writeInt(s.length());
		dos.writeBytes(s);
	}
	private String readString(DataInputStream dis) throws IOException {
		int size = dis.readInt();
		byte[] b = dis.readNBytes(size);
		return new String(b);
	}
	
	
	@Override
	public void save(OutputStream out) throws JerboaSerializeException,JerboaException {
		try {
		monitor.setMessage("Saving...");
		
		int taille = modeler.getGMap().size();
		int sizeProps = factory.properties().size();
		
		int count = taille + (taille*modeler.countEbd()) + sizeProps + 1 ;
		monitor.setMinMax(0, count);
		int progress = 0;
		monitor.setProgressBar(progress++);
		monitor.setMessage("Writing header...");
		
		DataOutputStream bos = new DataOutputStream(out);
		bos.writeBytes("JBB");
		bos.writeInt(1);
		writeString(bos, modeler.getClass().getName());
		dimension = modeler.getDimension();
		bos.writeInt(dimension);
		ebdlength = modeler.countEbd();
		bos.writeInt(ebdlength);
				
		for (JerboaEmbeddingInfo info : modeler.getAllEmbedding()) {
			bos.writeInt(info.getID());
			writeString(bos, info.getName());
			writeString(bos,info.getOrbit().toString());
			writeString(bos,info.getType().getName());
		}
		monitor.setProgressBar(progress++);
		
		// topology part
		monitor.setMessage("Writing topology...");
		JerboaGMap gmap = modeler.getGMap();
		gmap.pack();
		bos.writeInt(gmap.size());
		bos.writeInt(gmap.getCapacity());
				
		for (JerboaDart node : gmap) {
			saveNode(bos,node);
			monitor.setProgressBar(progress++);
		}
		
		monitor.setMessage("Writing embedding values...");
		for (JerboaEmbeddingInfo info : modeler.getAllEmbedding()) {
			JerboaMark marker = gmap.creatFreeMarker();
			writeString(bos,info.getName());
			for (JerboaDart node : gmap) {
				List<Integer> iddarts = new ArrayList<>();
				if(node.isNotMarked(marker)) {
					Object value = node.ebd(info.getID());
					if(value != null) {
						Collection<JerboaDart> orbit = gmap.markOrbit(node, info.getOrbit(), marker);
						for (JerboaDart n : orbit) {
							iddarts.add(n.getID());
						}
						
						bos.writeInt(iddarts.size());
						for (Integer integer : iddarts) {
							bos.writeInt(integer);
						}
						
						byte[] seq = factory.serialize(info, value);
						bos.writeInt(seq.length);
						bos.write(seq);
						// writeString(bos, seq.toString());
					}
				}
				monitor.setProgressBar(progress++);
			}
			bos.writeInt(-1);
			gmap.freeMarker(marker);
		}
		
		/*if(sizeProps > 0) {
			monitor.setMessage("Additional properties...");
			List<Pair<String,String>> props = factory.properties();
			dos.println("properties "+sizeProps);
			for (Pair<String, String> pair : props) {
				dos.println(pair.l());
				dos.println(pair.r());
			}
		}*/
		
		monitor.setMessage("Save operation finished...");
		bos.flush();
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new JerboaSerializeException(e);
		}
	}
	
	@Override
	public void load(InputStream input) throws JerboaSerializeException, JerboaException {
		
		int max = 4;
		int progress = 0;
		
		monitor.setMessage("Loading...");
		
		monitor.setMinMax(0, max);
		monitor.setProgressBar(progress);
		JerboaGMap gmap = modeler.getGMap();
		
		
		try
		{
			DataInputStream dis = new DataInputStream(input);
			monitor.setMessage("Reading header...");
			byte[] magicnumber = dis.readNBytes(3);
			int version = dis.readInt();
			
			String modelername = readString(dis);
			dimension = dis.readInt();
			ebdlength = dis.readInt();
			max+=ebdlength;
			HashMap<String, JerboaEmbeddingInfo> ebds = new HashMap<>();
			for(int i=0;i < ebdlength;i++) {
				int eid = dis.readInt();
				String ebdname = readString(dis);
				String ebdorbit = readString(dis);
				String ebdtype = readString(dis);
				String line = "ebdid "+ eid + " " + ebdname + " " + ebdorbit + " " + ebdtype;
				checkEbdInfo(ebds, line);
			}
			
			monitor.setProgressBar(progress++);
			monitor.setMessage("Preparing gmap...");
			
			int size = dis.readInt();
			int capacity = dis.readInt();
			JerboaDart[] newnodes = gmap.addNodes(size);
			int countnodes = newnodes.length; 
			
			monitor.setProgressBar(progress++);
			max+=countnodes;
			monitor.setMinMax(0, max);
			
			monitor.setMessage("Loading topology...");
			for(int i=0;i< countnodes;i++) {
				loadNode(newnodes,dis);
				monitor.setProgressBar(progress++);
			}
			
			monitor.setMessage("Loading embedding values...");
			
			max+=ebdlength;
			monitor.setMinMax(0, max);
			for (int i=0;i < ebdlength;i++) {
				String ebdname = readString(dis);
				JerboaEmbeddingInfo info = ebds.containsKey(ebdname)? ebds.get(ebdname) : null; // factory.getEmbeddingInfo(ebdname);
				int nbdarts = dis.readInt();
				max += nbdarts;
				monitor.setMinMax(0, max);
				while(nbdarts != -1) {
					List<Integer> ids = new ArrayList<>(nbdarts);
					for(int j = 0; j < nbdarts; ++j) {
						int did = dis.readInt();
						ids.add(did);
					}
					int length = dis.readInt();
					byte[] ebdvaluestr =  dis.readNBytes(length); // readString(dis);
					Object ebdvalue = factory.unserialize(info, ebdvaluestr);
					ids.stream().forEach(id -> newnodes[id].setEmbedding(info.getID(), ebdvalue));
					monitor.setProgressBar(progress++);
					nbdarts = dis.readInt(); // on lit la prochaine orbite
				}
				monitor.setProgressBar(progress++);
			}
			
			/*
			if(version >= 2) {
				monitor.setMessage("Additional properties...");
				line = reader.readLine();
				StringTokenizer tokenizer = new StringTokenizer(line);
				tokenizer.nextToken(); // keyword properties
				int sizeProps = Integer.parseInt(tokenizer.nextToken());
				
				max+=sizeProps;
				monitor.setMinMax(0, max);
				
				for (int i = 0;i < sizeProps; i++) {
					String propname = reader.readLine();
					String propvalue = reader.readLine();
					factory.setproperty(propname, propvalue);
					monitor.setProgressBar(progress++);
				}
			}*/
			
			
			monitor.setMessage("Completing loading...");
			
			
			List<JerboaDart> lnewnodes = Arrays.asList(newnodes);
			factory.completeProcess(modeler.getGMap(), lnewnodes);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch(Throwable t) {
			t.printStackTrace();
		}
		
		monitor.setMessage("Load operation terminated...");
	}
	
	private void loadNode(JerboaDart[] newnodes, DataInputStream dis) throws IOException {
		int nid = dis.readInt();
		JerboaDart node = newnodes[nid];
		for(int i=0;i <= dimension;i++) {
			int vid = dis.readInt();
			JerboaDart voisin = newnodes[vid];
			node.setAlpha(i, voisin);
		}
	}


	private void checkEbdInfo(HashMap<String, JerboaEmbeddingInfo> ebds, String line) throws JerboaSerializeException {

		Pattern pattern = Pattern.compile("ebdid (?<ID>\\d+) (?<NAME>\\w+) (?<ORBIT><\\s*((a\\d+)(\\s*,\\s*a\\d+)*)?\\s*>) (?<TYPE>[a-zA-Z0-9.:]+)\\s*$");
		Matcher matcher = pattern.matcher(line);
		boolean b = matcher.matches();
		if(b) {
			// int ebdid = Integer.parseInt(matcher.group("ID"));
			String name = matcher.group("NAME");
			JerboaOrbit orbit = JerboaOrbit.parseOrbit(matcher.group("ORBIT"));
			String type = matcher.group("TYPE");
			JerboaEmbeddingInfo ebd = factory.searchCompatibleEmbedding(name, orbit, type);
			if(ebd != null)
				ebds.put(name, ebd);
		}
		else
			throw new JerboaSerializeException("MalFormed EBD description: "+line);
	}


	private void extractCountEbd(String line) throws JerboaSerializeException {
		StringTokenizer tokenizer = new StringTokenizer(line);
		tokenizer.nextToken();
		ebdlength = Integer.parseInt(tokenizer.nextToken());
	}


	private void checkName(String line) {
		
	}


	private void affectDimension(String line) throws JBAIncompatibleDimensionException {
		StringTokenizer tokenizer = new StringTokenizer(line);
		tokenizer.nextToken();
		String dim = tokenizer.nextToken();
		dimension = Integer.parseInt(dim);
		if(!factory.manageDimension(dimension))
			throw new JBAIncompatibleDimensionException();
	}


	private void saveNode(DataOutputStream dos, JerboaDart node) throws IOException {
		dos.writeInt(node.getID());
		for(int i=0;i <= dimension;i++) {
			dos.writeInt(node.alpha(i).getID());
			
			/*for(int e=0;e < ebdlength;e++) {
				int idx = -1;
				Object o = node.getEmbedding(e).getValue();
				if(o==null) {
					dos.print(" ");
					dos.print(idx);
				}
				else {
					idx = values.indexOf(o);
					if(idx == -1) {
						idx = values.size();
						values.add(new Pair<JerboaEmbeddingInfo, Object>(modeler.getAllEmbedding().get(e), o));
					}
					dos.print(" ");
					dos.print(idx);
				}
			}*/
		}
	}

	

	public static void main(String[] args) {
		String line = "ebdid 0 point <a1, a2, a3> fr.up.xlim.sic.ig.jerboa.modeler.basic.embedding.Point3";
		
		Pattern pattern = Pattern.compile("ebdid (?<ID>\\d+) (?<NAME>\\w+) (?<ORBIT><\\s*((a\\d+)(\\s*,\\s*a\\d+)*)?\\s*>) (?<TYPE>[a-zA-Z0-9.]+)\\s*$");
		Matcher matcher = pattern.matcher(line);
		boolean b = matcher.matches();
		System.out.println("MATCHES? "+b);
		if(b) {
			int gr = matcher.groupCount();
			System.out.println("Group count: "+ gr);
			for(int i=0;i <= gr; i++) {
				System.out.println("["+i+"] -> "+matcher.group(i));
			}
			System.out.println("ID: "+matcher.group("ID"));
			System.out.println("NAME: "+matcher.group("NAME"));
			System.out.println("ORBIT: "+matcher.group("ORBIT"));
			System.out.println("TYPE: "+matcher.group("TYPE"));
		}
	}
}
