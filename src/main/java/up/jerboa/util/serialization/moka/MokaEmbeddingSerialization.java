/**
 * 
 */
package up.jerboa.util.serialization.moka;

import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.util.serialization.EmbeddingSerialization;

/**
 * @author hakim
 *
 */
public interface MokaEmbeddingSerialization extends EmbeddingSerialization {
	Object unserialize(JerboaEmbeddingInfo ebdpoint, MokaPoint mpoint);
	MokaPoint serialize(JerboaEmbeddingInfo ebdpoint, Object ebd);
}
