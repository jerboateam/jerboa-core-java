package up.jerboa.util.serialization.moka;

class MokaLine {
	int id;
	int[] links;
	
	float[] pts;

	MokaLine(int id, int[] links, float[] pts) {
		this.id = id;
		this.links = links;
		this.pts = pts;
	}

	MokaLine(int id, int[] links) {
		this(id, links, null);
	}
	
	boolean hasPoints() {
		return pts != null;
	}
}