package up.jerboa.util.serialization.objfile;

import java.util.Comparator;

import up.jerboa.core.util.Pair;

public class DistOBJPointComparator<T> implements
		Comparator<Pair<OBJPoint, T>> {

	private OBJPoint ref;
	private OBJPoint vref;
	
	public DistOBJPointComparator(OBJPoint ref, OBJPoint vectref) {
		this.ref = ref;
		this.vref = vectref;
	}
	@Override
	public int compare(Pair<OBJPoint, T> a, Pair<OBJPoint, T> b) {
		OBJPoint RA = new OBJPoint(ref, a.l());
		OBJPoint RB = new OBJPoint(ref, b.l());
		double dotRA = vref.dot(RA);
		double dotRB = vref.dot(RB);
		return Double.compare(dotRA, dotRB); 
	}

}
