package up.jerboa.util.serialization.objfile;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaRuleOperation;
import up.jerboa.exception.JerboaException;

public interface OBJBridge {
	
	JerboaDart[] makeFace(OBJPointProvider owner, Face face) throws JerboaException;
	
	JerboaDart[] extrudeA3(JerboaDart hook) throws JerboaException;
	
	JerboaRuleOperation getRuleSewA2();

	boolean hasOrient();
	
	boolean getOrient(JerboaDart node);
	
	OBJPoint getPoint(JerboaDart node);
	
	OBJPoint getNormal(JerboaDart node);
	void computeOrientNormal() throws JerboaException;
	
	/**
	 * Insere un sommet sur une arete referencee par v a la coordonnee point et renvoie le premier brin insere.
	 */
	JerboaDart insertVertex(JerboaDart v, OBJVertex point) throws JerboaException;

	List<JerboaDart> callCutFace(JerboaDart left, OBJVertex vdeb,
			JerboaDart right, OBJVertex vfin) throws JerboaException;

	/**
	 * 
	 * @param a
	 */
	void removeEdge(JerboaDart a);	
	// void multipleCutEdge(JerboaNode node, List<OBJVertex> newvertices) throws JerboaException;

	void callUnsewA2(JerboaDart node)throws JerboaException;

	void deleteConnex(JerboaDart node) throws JerboaException;

	void prepareGMap(JerboaGMap gmap) throws JerboaException;

	InputStream searchMTLFile(String filename) throws FileNotFoundException;

	void collapseEdge(JerboaDart alpha, OBJVertex vdeb, OBJVertex vfin) throws JerboaException;
	
	void complete();

	
}
