package up.jerboa.util.serialization.objfile;

import up.jerboa.util.serialization.EmbeddingSerialization;

public interface OBJEmbeddingSerialization extends EmbeddingSerialization {

		OBJBridge getBridge();
}
