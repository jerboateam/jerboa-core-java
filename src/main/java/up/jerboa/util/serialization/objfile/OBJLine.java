package up.jerboa.util.serialization.objfile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import up.jerboa.core.JerboaDart;

public class OBJLine implements Iterable<OBJPointDecoupe> {
	private ArrayList<OBJPointDecoupe> croisements;
	private JerboaDart faceA;
	private JerboaDart faceB;
	
	public OBJLine(JerboaDart faceA, JerboaDart faceB) {
		this.faceA = faceA;
		this.faceB = faceB;
		croisements = new ArrayList<>(10);
	}
	
	public List<OBJPointDecoupe> getCroisements() {
		return croisements;
	}
	
	public JerboaDart getFaceA() {
		return faceA;
	}

	public JerboaDart getFaceB() {
		return faceB;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Line: ").append(faceA.getID()).append("x").append(faceB.getID()).append(" -> ").append(croisements).append("\n");
		return sb.toString();
	}
	
	public boolean contains(OBJPoint point) {
		return croisements.contains(point);
	}

	public void add(OBJPointDecoupe ptdecoup) {
		if(!croisements.contains(ptdecoup))
			croisements.add(ptdecoup);
	}

	public void sortCroisements() {
		if(croisements.size() >= 2) {
			OBJPoint ref = croisements.get(0).getPoint();
			OBJPoint b = croisements.get(1).getPoint();
			OBJPoint vref = new OBJPoint(ref,b); // OBJPoint.vectorNormalize(croisements.get(1).getPoint(),ref);
			vref.normalize();
			Collections.sort(croisements, new OBJPointDecoupeComparator(ref,vref));
		}
	}

	public void addAll(List<OBJPointDecoupe> buf) {
		croisements.addAll(buf);
	}

	public int size() {
		return croisements.size();
	}

	@Override
	public Iterator<OBJPointDecoupe> iterator() {
		return croisements.iterator();
	}

}
