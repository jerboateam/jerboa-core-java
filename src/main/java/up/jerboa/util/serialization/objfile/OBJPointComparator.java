package up.jerboa.util.serialization.objfile;

import java.util.Comparator;

public class OBJPointComparator implements
		Comparator<OBJPoint> {

	private OBJPoint ref;
	private OBJPoint vref;
	
	public OBJPointComparator(OBJPoint ref, OBJPoint vectref) {
		this.ref = ref;
		this.vref = vectref;
	}
	@Override
	public int compare(OBJPoint a, OBJPoint b) {
		OBJPoint RA = new OBJPoint(ref, a);
		OBJPoint RB = new OBJPoint(ref, b);
		double dotRA = vref.dot(RA);
		double dotRB = vref.dot(RB);
		
		if(dotRA < 0)
			dotRA = -RA.norm();
		else
			dotRA = RA.norm();
		
		if(dotRB < 0)
			dotRB = -RB.norm();
		else
			dotRB = RB.norm();
		
		
		return Double.compare(dotRA, dotRB); 
	}

}
