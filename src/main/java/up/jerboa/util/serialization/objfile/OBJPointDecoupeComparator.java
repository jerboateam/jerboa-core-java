package up.jerboa.util.serialization.objfile;

import java.util.Comparator;

public class OBJPointDecoupeComparator implements
		Comparator<OBJPointDecoupe> {

	private OBJPoint ref;
	private OBJPoint vref;
	
	public OBJPointDecoupeComparator(OBJPoint ref, OBJPoint vectref) {
		this.ref = ref;
		this.vref = vectref;
	}
	@Override
	public int compare(OBJPointDecoupe a, OBJPointDecoupe b) {
		OBJPoint RA = new OBJPoint(ref, a.getPoint());
		OBJPoint RB = new OBJPoint(ref, b.getPoint());
		double dotRA = vref.dot(RA);
		double dotRB = vref.dot(RB);
		
		if(dotRA < 0)
			dotRA = -RA.norm();
		else
			dotRA = RA.norm();
		
		if(dotRB < 0)
			dotRB = -RB.norm();
		else
			dotRB = RB.norm();
		
		
		return Double.compare(dotRA, dotRB); 
	}

}
