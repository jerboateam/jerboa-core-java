package up.jerboa.util.serialization.objfile;

import java.util.Comparator;

import up.jerboa.core.JerboaDart;

class OrientDemiFaceComparator implements Comparator<JerboaDart> {

	/**
	 * 
	 */
	private OBJPoint refzero;
	private OBJPoint base;
	private OBJBridge converter;
		
	public OrientDemiFaceComparator(OBJBridge converter, JerboaDart edge) {
		this.converter = converter;
		
		base = convEdgeNodeVecteurOrient(edge);
		// refzero = computeNormal(edge);// convEdgeNodeVecteurOrientRec(edge.alpha(1));
		refzero = converter.getNormal(edge);
		this.refzero.normalize();
		this.base.normalize();
	}
		
	private OBJPoint convEdgeNodeVecteurOrient(JerboaDart n) {
		OBJPoint a;
		OBJPoint b;
		if(converter.getOrient(n)) {
			a = converter.getPoint(n);
			b = converter.getPoint(n.alpha(0));
		}
		else {
			a = converter.getPoint(n.alpha(0));
			b = converter.getPoint(n);
		}
		OBJPoint ab = new OBJPoint(a,b);
		return ab;
	}
	
	private OBJPoint computeNormal(JerboaDart first) {
		OBJPoint ab;
		OBJPoint bc;
		
		if(!converter.getOrient(first))
			first = first.alpha(3);
		JerboaDart node = first;
		
		do {
			OBJPoint a = converter.getPoint(node);
			OBJPoint b = converter.getPoint(node.alpha(0));
			OBJPoint c = converter.getPoint(node.alpha(0).alpha(1).alpha(0));

			ab = new OBJPoint(a, b);
			bc = new OBJPoint(b, c);
			node = node.alpha(0).alpha(1);
			if(node == first)
				return new OBJPoint(1, 1, 1); // point de sortie...
		} while (ab.isColinear(bc));

		OBJPoint normal = ab.cross(bc);
		normal.normalize();

		return normal;
	}
	
	
	@Override
	public int compare(JerboaDart o1, JerboaDart o2) {
		// OBJPoint v1 = computeNormal(o1); // convEdgeNodeVecteurOrientRec(o1.alpha(1));
		// OBJPoint v2 = computeNormal(o2); // convEdgeNodeVecteurOrientRec(o2.alpha(1));
		OBJPoint v1 = converter.getNormal(o1);
		OBJPoint v2 = converter.getNormal(o2);
		double angle1 =  base.angle(refzero, v1);
		double angle2 = base.angle(refzero, v2);
		
		return Double.compare(angle1, angle2);
	}

	private OBJPoint convEdgeNodeVecteurOrientRec(JerboaDart n) {
		OBJPoint ab;
		do {
			ab = convEdgeNodeVecteurOrient(n);
			n = n.alpha(0).alpha(1);
		} while(ab.isColinear(base));
		return ab;
	}
	
}