package up.jerboa.util.serialization.offfile;

import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaRuleOperation;
import up.jerboa.exception.JerboaException;

public interface OFFBridge {
	
	JerboaDart[] makeFace(OFFParser owner, Face face) throws JerboaException;
	
	JerboaDart[] extrudeA3(JerboaDart hook) throws JerboaException;
	
	JerboaRuleOperation getRuleSewA2();

	boolean hasOrient();
	
	boolean getOrient(JerboaDart node);
	
	OFFPoint getPoint(JerboaDart node);
	
}
