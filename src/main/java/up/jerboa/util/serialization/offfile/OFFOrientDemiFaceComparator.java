package up.jerboa.util.serialization.offfile;

import java.util.Comparator;

import up.jerboa.core.JerboaDart;

class OFFOrientDemiFaceComparator implements Comparator<JerboaDart> {

	/**
	 * 
	 */
	private OFFPoint orient;
	private OFFPoint base;
	private OFFBridge converter;
		
	public OFFOrientDemiFaceComparator(OFFBridge converter, JerboaDart edge) {
		this.converter = converter;
		
		base = convEdgeNodeVecteurOrient(edge);
		orient = computeNormal(edge);// convEdgeNodeVecteurOrientRec(edge.alpha(1));
		
		this.orient.normalize();
		this.base.normalize();
	}
		
	private OFFPoint convEdgeNodeVecteurOrient(JerboaDart n) {
		OFFPoint a = converter.getPoint(n);
		OFFPoint b = converter.getPoint(n.alpha(0));
		OFFPoint ab = new OFFPoint(a,b);
		return ab;
	}
	
	private OFFPoint computeNormal(JerboaDart first) {
		OFFPoint ab;
		OFFPoint bc;
		JerboaDart node = first;
		do {
			OFFPoint a = converter.getPoint(node);
			OFFPoint b = converter.getPoint(node.alpha(0));
			OFFPoint c = converter.getPoint(node.alpha(0).alpha(1).alpha(0));

			ab = new OFFPoint(a, b);
			bc = new OFFPoint(b, c);
			node = node.alpha(0).alpha(1);
			if(node == first)
				return new OFFPoint(1, 1, 1); // point de sortie...
		} while (ab.isColinear(bc));

		OFFPoint normal = ab.cross(bc);
		normal.normalize();

		return normal;
	}
	
	
	@Override
	public int compare(JerboaDart o1, JerboaDart o2) {
		OFFPoint v1 = computeNormal(o1); // convEdgeNodeVecteurOrientRec(o1.alpha(1));
		OFFPoint v2 = computeNormal(o2); // convEdgeNodeVecteurOrientRec(o2.alpha(1));
		double angle1 =  orient.angle(base, v1);
		double angle2 = orient.angle(base, v2);
		
		return Double.compare(angle1, angle2);
	}
	
}