package up.jerboa.util.serialization.vtk.ugrid;

public class VTKPoint {
	public double x;
	public double y;
	public double z;
	
	public VTKPoint(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public String toString() {
		return ""+x+" "+y+" "+z;
	}
	
}
