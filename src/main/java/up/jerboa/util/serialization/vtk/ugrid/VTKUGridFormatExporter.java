package up.jerboa.util.serialization.vtk.ugrid;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import up.jerboa.algo.cells.CellExtractor;
import up.jerboa.algo.cells.Edge;
import up.jerboa.algo.cells.Face;
import up.jerboa.algo.cells.Vertex;
import up.jerboa.algo.cells.Volume;
import up.jerboa.core.JerboaDart;
import up.jerboa.core.JerboaEmbeddingInfo;
import up.jerboa.core.JerboaGMap;
import up.jerboa.core.JerboaModeler;
import up.jerboa.core.JerboaOrbit;
import up.jerboa.exception.JerboaException;
import up.jerboa.util.JerboaSerializerMonitor;
import up.jerboa.util.serialization.JerboaExportAdapter;
import up.jerboa.util.serialization.JerboaSerializeException;

/**
 * 
 * @author romain
 *
 */
public class VTKUGridFormatExporter extends JerboaExportAdapter<VTKUGridEmbeddingSerialization> {

	public VTKUGridFormatExporter(JerboaModeler modeler,
			JerboaSerializerMonitor monitor,
			VTKUGridEmbeddingSerialization factory) {
		super(modeler, monitor, factory, ".vtk");
	}

	@Override
	public void save(OutputStream is)
			throws JerboaSerializeException, JerboaException {
		writeGMapToVTK(new PrintStream(is));
		
	}

	final public void writeGMapToVTK(PrintStream ps) throws JerboaSerializeException, JerboaException {

		
		if(modeler.getDimension() > 3)
			throw new JerboaSerializeException();
		JerboaGMap gmap = modeler.getGMap();
		JerboaEmbeddingInfo ebdpoint = factory.searchCompatibleEmbedding("point", JerboaOrbit.orbit(1,2,3), null);

		CellExtractor extractor = new CellExtractor(gmap);
		
		// Now we can use the Gmap
//		System.err.println(modeler.getGMap());
//
//		for (JerboaDart dart : modeler.getGMap()) {
//			System.out.println(dart);
//			System.out.println(dart.getEmbedding(0));
//		}
//		
		// header
		writeHeader(ps);
		
		// points
		List<Vertex> vertices = extractor.getVertices();
		ps.println("\nPOINTS " + vertices.size() + " double");
		for (Vertex vertex : vertices) {
			Object position = vertex.getDart().getEmbedding(0);
			VTKPoint point = factory.serialize(ebdpoint, position);
			ps.println(point.toString());
		}
		
		int size = 0;
		int nbCells = 0;
		StringBuilder sb = new StringBuilder();
		
		// edges
		Collection<Edge> edges = extractor.getEdges();
		nbCells += edges.size();
		for (Edge edge : edges) {
			size += writeEdge(sb, edge);
		}
		
		// faces
		Collection<Face> faces = extractor.getFaces();
		nbCells += faces.size();
		for (Face face : faces) {
			size += writePolygon(sb, face);
		}
		
		// volumes
		Collection<Volume> volumes = extractor.getVolumes();
		nbCells += volumes.size();
		for (Volume volume : volumes) {
			size += writePolyhedron(sb, volume);
		}
		
		ps.println("\nCELLS " + nbCells + " " + size);
		ps.println(sb.toString());
		
		// Print cell types.
		ps.println("CELL_TYPES " + nbCells);
		for (int e=0; e<edges.size(); e++) {
			// TYPE=3 for VTK_LINE
			ps.println(3);
		}
		for (int f=0; f<faces.size(); f++) {
			// TYPE=7 for VTK_POLYGON
			ps.println(7);
		}
		for (int v=0; v<volumes.size(); v++) {
			// TYPE=42 for VTK_POLYHEDRON
			ps.println(42);
		}
		
	}

	private void writeHeader(PrintStream ps) {
		ps.println("# vtk DataFile Version 2.0");
		ps.println("Jerboa export");
		ps.println("\nASCII\nDATASET UNSTRUCTURED_GRID");
	}
	

	private int writeEdge(StringBuilder sb,
			Edge edge) {
		sb.append(edge + "\n");
		return 3;
	}
	

	private int writePolygon(StringBuilder sb,
			Face face) {
		sb.append(face + "\n");
		return 1 + face.size(); // 1 + #vertices
	}
	
	private int writePolyhedron(StringBuilder sb,
			Volume volume) {
		/*
		 * num_items num_faces num_face_1 v0 v1 v2 ... num_face_N v0 v1 v2 ...
		 */
		StringBuilder sbPolyh = new StringBuilder();		
		sbPolyh.append(volume.size());
		int sizePolyh = 1; // nb_faces
		for (Face face : volume) {
			sbPolyh.append(" " + face);
			sizePolyh++;
			sizePolyh += face.size();
		}
		sbPolyh.append("\n");
		sb.append(sizePolyh + " " + sbPolyh.toString());
		sizePolyh++; // nb_items
		return sizePolyh; // 2 + #faces + #vertices for each face
	}

}
