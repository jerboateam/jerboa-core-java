/**
 * 
 */
package up.jerboa.core;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import up.jerboa.core.util.JerboaModelerGeneric;

/**
 * @author Hakim Belhaouari
 *
 */
public class JerboaNodeTest {

	JerboaModelerGeneric modeler;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		modeler = new JerboaModelerGeneric(2);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link up.jerboa.core.JerboaDart#JerboaNode(int, int)}.
	 */
	@Test
	public final void testJerboaNodeIntInt() {
		try {
			new JerboaDart(modeler.getGMap(), 0);
		}
		catch(Throwable t) {
			fail("Constructor node failed");
		}
	}

	/**
	 * Test method for {@link up.jerboa.core.JerboaDart#alpha(int)}.
	 */
	@Test
	public final void testAlpha() {
		JerboaDart node1 = new JerboaDart(modeler.getGMap(), 0);
		
		
		int dim = modeler.getDimension();
		for(int i = 0;i <= dim; ++i) {
			assertTrue(node1.alpha(i) == node1);
		}
		
	}

	/**
	 * Test method for {@link up.jerboa.core.JerboaDart#setAlpha(int, up.jerboa.core.JerboaDart)}.
	 */
	@Test
	public final void testSetAlpha() {
		JerboaDart node1 = new JerboaDart(modeler.getGMap(), 0);
		JerboaDart node2 = new JerboaDart(modeler.getGMap(), 1);
		
		int dim = modeler.getDimension();
		
		for(int i = 0; i <= dim; ++i) {
			node1.setAlpha(i, node2);
			for(int j = 0; j <= i; ++j) {
				assertTrue(node1.alpha(j) == node2); assertTrue(node2.alpha(j) == node1);
			}
			for(int k = i+1;k <= dim; k++ ) {
				assertTrue(node1.alpha(k) == node1); assertTrue(node2.alpha(k) == node2);
			}
		}
		
		
		/*node1.setAlpha(1, node2);
		assertTrue(node1.alpha(0) == node2); assertTrue(node2.alpha(0) == node1);
		assertTrue(node1.alpha(1) == node2); assertTrue(node2.alpha(1) == node1);
		assertTrue(node1.alpha(2) == node1); assertTrue(node2.alpha(2) == node2);
		assertTrue(node1.alpha(3) == node1); assertTrue(node2.alpha(3) == node2);
		
		node1.setAlpha(2, node2);
		assertTrue(node1.alpha(0) == node2); assertTrue(node2.alpha(0) == node1);
		assertTrue(node1.alpha(1) == node2); assertTrue(node2.alpha(1) == node1);
		assertTrue(node1.alpha(2) == node2); assertTrue(node2.alpha(2) == node1);
		assertTrue(node1.alpha(3) == node1); assertTrue(node2.alpha(3) == node2);
		
		node1.setAlpha(3, node2);
		assertTrue(node1.alpha(0) == node2); assertTrue(node2.alpha(0) == node1);
		assertTrue(node1.alpha(1) == node2); assertTrue(node2.alpha(1) == node1);
		assertTrue(node1.alpha(2) == node2); assertTrue(node2.alpha(2) == node1);
		assertTrue(node1.alpha(3) == node2); assertTrue(node2.alpha(3) == node1);
		*/
	}
	
	/**
	 * Test method for {@link up.jerboa.core.JerboaDart#setMultipleAlpha(Object...)}.
	 */
	@Test
	public final void testSetMultipleAlpha() {
		{// test en definition complete
			JerboaGMap gmap = new JerboaGMapArray(modeler,2);
			JerboaDart a = gmap.addNode();
			JerboaDart b = gmap.addNode();
			JerboaDart c = gmap.addNode();
			JerboaDart d = gmap.addNode();
			JerboaDart e = gmap.addNode();
			JerboaDart f = gmap.addNode();
			JerboaDart g = gmap.addNode();
			JerboaDart h = gmap.addNode();

			a.setMultipleAlpha(0,b,1,h);
			b.setMultipleAlpha(1,c,0,a);
			c.setMultipleAlpha(0,d,1,b);
			d.setMultipleAlpha(0,c,1,e);
			e.setMultipleAlpha(0,f,1,d);
			f.setMultipleAlpha(0,e,1,g);
			g.setMultipleAlpha(0,h,1,f);
			h.setMultipleAlpha(0,g,1,a);
			
			assertTrue(a.alpha(0) == b); assertTrue(a.alpha(1) == h);
			assertTrue(b.alpha(0) == a); assertTrue(b.alpha(1) == c);
			assertTrue(c.alpha(0) == d); assertTrue(c.alpha(1) == b);
			assertTrue(d.alpha(0) == c); assertTrue(d.alpha(1) == e);
			assertTrue(e.alpha(0) == f); assertTrue(e.alpha(1) == d);
			assertTrue(f.alpha(0) == e); assertTrue(f.alpha(1) == g);
			assertTrue(g.alpha(0) == h); assertTrue(g.alpha(1) == f);
			assertTrue(h.alpha(0) == g); assertTrue(h.alpha(1) == a);
		}
		{
			// test en definition minimale (verif liaison "reverse")
			JerboaGMap gmap = new JerboaGMapArray(modeler,2);
			JerboaDart a = gmap.addNode();
			JerboaDart b = gmap.addNode();
			JerboaDart c = gmap.addNode();
			JerboaDart d = gmap.addNode();
			JerboaDart e = gmap.addNode();
			JerboaDart f = gmap.addNode();
			JerboaDart g = gmap.addNode();
			JerboaDart h = gmap.addNode();
			
			a.setMultipleAlpha(0,b,1,h);
			c.setMultipleAlpha(0,d,1,b);
			e.setMultipleAlpha(0,f,1,d);
			g.setMultipleAlpha(0,h,1,f);

			assertTrue(a.alpha(0) == b); assertTrue(a.alpha(1) == h);
			assertTrue(b.alpha(0) == a); assertTrue(b.alpha(1) == c);
			assertTrue(c.alpha(0) == d); assertTrue(c.alpha(1) == b);
			assertTrue(d.alpha(0) == c); assertTrue(d.alpha(1) == e);
			assertTrue(e.alpha(0) == f); assertTrue(e.alpha(1) == d);
			assertTrue(f.alpha(0) == e); assertTrue(f.alpha(1) == g);
			assertTrue(g.alpha(0) == h); assertTrue(g.alpha(1) == f);
			assertTrue(h.alpha(0) == g); assertTrue(h.alpha(1) == a);
		}
	}

	

}
